﻿using LSIS.DATA;
using LSIS.ENTITY;
using LSIS.SERVICES.Interfaces;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace LSIS.SERVICES
{
    public partial class RoleModulesOptionServices : IBasicMethods<RoleModulesOption>
    {
        #region [Static Properties]
        public static RoleModulesOptionServices Current { get { return new RoleModulesOptionServices(); } }
        #endregion

        #region [Constructs]
        private void Construct(RoleModulesOption module)
        {

        }

        private void Construct(IEnumerable<RoleModulesOption> modules)
        {
            foreach (RoleModulesOption option in modules)
            {
                option.Module = GeneralModuleAdapter.Current.Find(x => x.GID == option.ModuleId).FirstOrDefault();

                option.Option = GeneralModuleAdapter.Current.Find(x => x.GID == option.OptionId).FirstOrDefault();
            }
        }

        #endregion

        public OperationResult<IEnumerable<RoleModulesOption>> FindByCompany(Guid companyId, bool isConstruct = false)
        {
            OperationResult<IEnumerable<RoleModulesOption>> result = new OperationResult<IEnumerable<RoleModulesOption>>();

            try
            {
                var userRoleGid = UsersRoleAdapter.Current.Find(x => x.CompanyId == companyId).Select(c => c.GID);

                var test = this.Find(x => userRoleGid.Contains(x.UserRoleId));

            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<IEnumerable<RoleModulesOption>> FindByUserId(Guid userId, bool isConstruct = false)
        {
            return this.Find(x => x.UserRoleId == userId, isConstruct);
        }

        public OperationResult<IEnumerable<RoleModulesOption>> Find(Expression<Func<RoleModulesOption, bool>> condition, bool isConstruct = false)
        {
            OperationResult<IEnumerable<RoleModulesOption>> result = new OperationResult<IEnumerable<RoleModulesOption>>();

            try
            {
                result.Success = true;

                result.Data = RoleModulesOptionAdapter.Current.Find(condition);

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<IEnumerable<RoleModulesOption>> GetAll(bool isConstruct = false)
        {
            OperationResult<IEnumerable<RoleModulesOption>> result = new OperationResult<IEnumerable<RoleModulesOption>>();

            try
            {
                result.Success = true;

                result.Data = RoleModulesOptionAdapter.Current.GetAll();

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<IEnumerable<RoleModulesOption>> Save(RoleModulesOption entity)
        {
            throw new NotImplementedException();
        }

        public OperationResult<IEnumerable<UsersRole>> Save(UsersRole entity)
        {
            OperationResult<IEnumerable<UsersRole>> result = new OperationResult<IEnumerable<UsersRole>>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    result.Success = true;

                    if (!UsersRoleAdapter.Current.Save(entity).Success) { throw new Exception("User Role can't be saved"); }

                    if (entity.Options != null)
                    {
                        if (!RoleModulesOptionAdapter.Current.Save(entity.Options).Success) { throw new Exception("User Role can't be saved."); }
                    }

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    result.SetFail(ex);
                    scope.Dispose();
                }
            }

            return result;
        }

        public OperationResult<IEnumerable<RoleModulesOption>> Save(IEnumerable<RoleModulesOption> entities)
        {
            OperationResult<IEnumerable<RoleModulesOption>> result = new OperationResult<IEnumerable<RoleModulesOption>>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    result = RoleModulesOptionAdapter.Current.Save(entities);

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    result.SetFail(ex);
                    scope.Dispose();
                }
            }

            return result;
        }
    }
}
