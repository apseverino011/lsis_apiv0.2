﻿using LSIS.ENTITY;
using LSIS.SERVICES.Interfaces;
using LSIS.UTILITY;
using LSIS.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace LSIS.SERVICES
{
    public partial class AccClassServices : IBasicMethods<AccClass>
    {
        #region Static Properties
        public static AccClassServices Current { get { return new AccClassServices(); } }
        #endregion

        public void Construct(AccClass _class)
        {
            this.Construct(new List<AccClass>() { _class });
        }

        public void Construct(IEnumerable<AccClass> classes)
        {
            foreach (var _cla in classes)
            {
                _cla.AccGroup = AccGroupAdapter.Current.Find(x => x.GID == _cla.AccGroupId).FirstOrDefault();
            }
        }

        public OperationResult<IEnumerable<AccClass>> GetAll(bool isConstruct = false)
        {
            OperationResult<IEnumerable<AccClass>> result = new OperationResult<IEnumerable<AccClass>>();

            try
            {
                result.Data = AccClassAdapter.Current.Find(x => x.CompanyId == InternalInfo.CompanyId);

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<AccClass> Find(Guid gid, bool isConstruct = false)
        {
            OperationResult<AccClass> result = new OperationResult<AccClass>();

            try
            {
                result.Data = this.Find(x => x.GID == gid).Data.FirstOrDefault();
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<IEnumerable<AccClass>> Find(Expression<Func<AccClass, bool>> condition, bool isConstruct = false)
        {
            OperationResult<IEnumerable<AccClass>> result = new OperationResult<IEnumerable<AccClass>>();

            try
            {
                result.Data = AccClassAdapter.Current.Find(condition).Where(x => x.CompanyId == InternalInfo.CompanyId);

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<IEnumerable<AccClass>> Save(AccClass entity)
        {

            return this.Save(new List<AccClass>() { entity });
        }

        public OperationResult<IEnumerable<AccClass>> Save(IEnumerable<AccClass> entities)
        {
            OperationResult<IEnumerable<AccClass>> result = new OperationResult<IEnumerable<AccClass>>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    entities.ToList().ForEach(ac => { ac.CompanyId = InternalInfo.CompanyId; });
                    result = AccClassAdapter.Current.Save(entities);

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    result.SetFail(ex);
                    scope.Dispose();
                }
            }

            return result;
        }
    }
}
