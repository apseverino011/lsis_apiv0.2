﻿using LSIS.DATA;
using LSIS.ENTITY;
using LSIS.SERVICES.Interfaces;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace LSIS.SERVICES
{
    public partial class PartyServices : IBasicMethods<Party>
    {
        #region [ Static Properties ]
        public static PartyServices Current { get { return new PartyServices(); } }
        #endregion

        #region [ Constructs]
        public void Construct(Party party)
        {
            this.Construct(new List<Party>() { party });
        }

        public void Construct(IEnumerable<Party> parties)
        {
            foreach (Party party in parties)
            {
                party.ContactNumbers = ContactNumberAdapter.Current.Find(x => x.OwnerId == party.GID);

                party.Identifications = PartyIdentificationAdapter.Current.Find(x => x.Party == party.GID);

                party.Identifications.ForEach(r =>
                {
                    r.IdentificationTypeName = GeneralTypeAdapter.Current.Find(x => x.GID == r.IdentificationType).FirstOrDefault().Description;
                });
            }
        }
        #endregion

        public OperationResult<IEnumerable<Party>> FindByIdent(string identNumber, bool isConstruct = false)
        {
            OperationResult<IEnumerable<Party>> result = new OperationResult<IEnumerable<Party>>();

            var partyGIDs = PartyIdentificationAdapter.Current.Find(x => x.Number.Contains(identNumber)).Select(c => c.Party);
        
            result.Data = PartyAdapter.Current.Find(x => partyGIDs.Contains(x.GID));

            return result;
        }

        public OperationResult<IEnumerable<Party>> Find(Guid gid, bool isConstruct = false)
        {
            return this.Find(x => x.GID == gid);
        }

        public OperationResult<IEnumerable<Party>> Find(string name, bool isConstruct = false)
        {
            OperationResult<IEnumerable<Party>> result = new OperationResult<IEnumerable<Party>>();

            try
            {
                string[] names = name.Split(' ');

                List<List<Party>> pLists = new List<List<Party>>();
                List<Party> Partys = new List<Party>();

                foreach (string n in name.Split(' ').Where(nn => nn != string.Empty && nn.Length > 2))
                {
                    pLists.Add(PartyAdapter.Current.Find(p => p.FullName.ToLower().Contains(n.ToLower())));
                }

                List<Party> tempParties = pLists.SelectMany(blp => blp).ToList();

                tempParties.ForEach(p =>
                {
                    if (Partys.Any(p2 => p2.GID.Equals(p.GID))) { return; }

                    Partys.Add(p);
                });


                result.Success = true;
                result.Data = Partys;

                if (isConstruct) { this.Construct(result.Data); }


            }
            catch (Exception ex)
            {
                result.SetFail(ex.InnerException);
                //ErrorLog.Current.SaverError("", "Party.Find by Name", "", ex.InnerException.ToString());
            }

            return result;
        }

        public OperationResult<IEnumerable<Party>> Find(Expression<Func<Party, bool>> condition, bool isConstruct = false)
        {

            OperationResult<IEnumerable<Party>> result = new OperationResult<IEnumerable<Party>>();

            try
            {
                result.Success = true;
                result.Data = PartyAdapter.Current.Find(condition);

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex.InnerException);
                //ErrorLog.Current.SaverError("", "Party.Find by Name", "", ex.InnerException.ToString());
            }

            return result;
        }

        public OperationResult<IEnumerable<Party>> GetAll(bool isConstruct = false)
        {
            OperationResult<IEnumerable<Party>> result = new OperationResult<IEnumerable<Party>>();

            try
            {
                result.Success = true;
                result.Data = PartyAdapter.Current.GetAll();

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex.InnerException);
                //ErrorLog.Current.SaverError("", "Party.Find by Name", "", ex.InnerException.ToString());
            }

            return result;
        }

        public OperationResult<IEnumerable<Party>> Save(Party entity)
        {
            return this.Save(new List<Party>() { entity });
        }

        public OperationResult<IEnumerable<Party>> Save(IEnumerable<Party> entities)
        {
            OperationResult<IEnumerable<Party>> result = new OperationResult<IEnumerable<Party>>();

            List<Party> parties = new List<Party>();
            List<ContactNumber> contactNumbers = new List<ContactNumber>();
            List<PartyIdentification> identifications = new List<PartyIdentification>();
            List<ContactPerson> contactPersons = new List<ContactPerson>();
            List<Party> partyContactPersons = new List<Party>();
            List<Location> locations = new List<Location>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    foreach (Party p in entities)
                    {
                        if (p.GID == null || p.GID == Guid.Empty) { p.GID = Guid.NewGuid(); }

                        parties.Add(p);

                        p.ContactNumbers.ForEach(x => { x.OwnerId = p.GID; });
                        contactNumbers.AddRange(p.ContactNumbers);

                        p.Identifications.ForEach(x => { x.Party = p.GID; });
                        identifications.AddRange(p.Identifications);
                        if (p.PersonsContact != null)
                        {
                            foreach (var par in p.PersonsContact)
                            {
                                if (par.GID == null || par.GID == Guid.Empty) { par.GID = Guid.NewGuid(); }

                                partyContactPersons.Add(par);

                                contactPersons.Add(new ContactPerson()
                                {
                                    OwnerId = p.GID,
                                    ContactId = par.GID
                                });
                            }
                        }

                        if (p.PartyLocations != null)
                        {
                            p.PartyLocations.ForEach(x => { x.OwnerId = p.GID; });
                            locations.AddRange(p.PartyLocations);
                        }

                    }

                    //Save Party
                    if (!PartyAdapter.Current.Save(parties).Success) { throw new Exception("Error has occurred in Party save"); }

                    //Save Contact Numbers
                    if (!ContactNumberAdapter.Current.Save(contactNumbers).Success) { throw new Exception("Error has occurred in Party ContactNumbers save"); }

                    //Save Identifications
                    if (!PartyIdentificationAdapter.Current.Save(identifications).Success) { throw new Exception("Error has occurred in Party Identifications save"); }

                    //Save Locations
                    if (!LocationAdapter.Current.Save(locations).Success) { throw new Exception("Error has occurred in Party Locations save"); }

                    //Save Party and Contact Person
                    if (!PartyAdapter.Current.Save(partyContactPersons).Success) { throw new Exception("Error has occurred in Party Contact Person save"); }

                    if (!ContactPersonsAdapter.Current.Save(contactPersons).Success) { throw new Exception("Error has occurred in Party Contact Person save"); }

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    result.SetFail(ex);
                    scope.Dispose();
                    //ErrorLog.Current.SaverError("", "Party.Find by Name", "", ex.InnerException.ToString());
                }
            }

            return result;
        }
    }
}
