﻿using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.SERVICES.Interfaces
{
    public interface IBasicMethods<T> where T : class
    {
        /// <summary>
        /// Get data by expression
        /// </summary>
        /// <param name="condition">Expression<Func<T, bool>></param>
        /// <returns>return list to T</returns>
        OperationResult<IEnumerable<T>> Find(Expression<Func<T, bool>> condition, bool isConstruct = false);
        /// <summary>
        /// Get all information on the DB for table T
        /// </summary>
        /// <returns>List<T> of rows</returns>
        OperationResult<IEnumerable<T>> GetAll(bool isConstruct = false);
        /// <summary>
        /// Saved changed on DB
        /// </summary>
        /// <returns></returns>
        //OperationResult<IEnumerable<T>> Save(T entity);
        /// <summary>
        /// Saved changed on DB
        /// </summary>
        /// <returns></returns>
        OperationResult<IEnumerable<T>> Save(IEnumerable<T> entities);
    }
}
