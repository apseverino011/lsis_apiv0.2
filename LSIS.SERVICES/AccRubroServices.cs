﻿using LSIS.DATA;
using LSIS.ENTITY;
using LSIS.SERVICES.Interfaces;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.SERVICES
{
    public partial class AccRubroServices : IBasicMethods<AccRubro>
    {
        #region Static Properties
        public static AccRubroServices Current { get { return new AccRubroServices(); } }
        #endregion

        public void Construct(AccRubro rubro)
        {
            this.Construct(new List<AccRubro>() { rubro });
        }

        public void Construct(IEnumerable<AccRubro> rubros)
        {
            foreach (var rubro in rubros)
            {
                rubro.AccGroup = AccGroupAdapter.Current.Find(c => c.GID == rubro.AccGroupGID).FirstOrDefault();
            }
        }


        public OperationResult<IEnumerable<AccRubro>> GetAll(bool isConstruct = false)
        {
            OperationResult<IEnumerable<AccRubro>> result = new OperationResult<IEnumerable<AccRubro>>();

            try
            {
                result.Data = AccRubroAdapter.Current.Find(x => x.CompanyId == InternalInfo.CompanyId);

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<AccRubro> Find(Guid gid, bool isConstruct = false)
        {
            OperationResult<AccRubro> result = new OperationResult<AccRubro>();

            try
            {
                result.Data = this.Find(x => x.GID == gid).Data.FirstOrDefault();

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<IEnumerable<AccRubro>> Find(Expression<Func<AccRubro, bool>> condition, bool isConstruct = false)
        {
            OperationResult<IEnumerable<AccRubro>> result = new OperationResult<IEnumerable<AccRubro>>();

            try
            {
                result.Data = AccRubroAdapter.Current.Find(condition).Where(x => x.CompanyId == InternalInfo.CompanyId);

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<IEnumerable<AccRubro>> Save(AccRubro entity)
        {
            return this.Save(new List<AccRubro>() { entity });
        }

        public OperationResult<IEnumerable<AccRubro>> Save(IEnumerable<AccRubro> entities)
        {
            OperationResult<IEnumerable<AccRubro>> result = new OperationResult<IEnumerable<AccRubro>>();

            try
            {
                entities.ToList().ForEach(r => { r.CompanyId = InternalInfo.CompanyId; });
            
                result = AccRubroAdapter.Current.Save(entities);

            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }
    }
}
