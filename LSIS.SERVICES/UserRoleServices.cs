﻿using LSIS.DATA;
using LSIS.ENTITY;
using LSIS.SERVICES.Interfaces;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.SERVICES
{
    public partial class UserRoleServices : IBasicMethods<UsersRole>
    {
        #region [Static Properties]
        public static UserRoleServices Current { get { return new UserRoleServices(); } }
        #endregion

        public void Construct(IEnumerable<UsersRole> usersRoles)
        {
            foreach (UsersRole role in usersRoles)
            {
                role.Options = RoleModulesOptionAdapter.Current.Find(x => x.UserRoleId == role.GID);
            }
        }

        public OperationResult<IEnumerable<UsersRole>> FindByCompany(Guid company, bool isConstruct = false)
        {
            return this.Find(x => x.CompanyId == company, isConstruct);
        }

        public OperationResult<IEnumerable<UsersRole>> Find(Expression<Func<UsersRole, bool>> condition, bool isConstruct = false)
        {
            OperationResult<IEnumerable<UsersRole>> result = new OperationResult<IEnumerable<UsersRole>>();

            try
            {
                result.Data = UsersRoleAdapter.Current.Find(condition);

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }


            return result;
        }

        public OperationResult<IEnumerable<UsersRole>> GetAll(bool isConstruct = false)
        {
            OperationResult<IEnumerable<UsersRole>> result = new OperationResult<IEnumerable<UsersRole>>();

            try
            {
                result.Data = UsersRoleAdapter.Current.GetAll();

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }


            return result;
        }

        public OperationResult<IEnumerable<UsersRole>> Save(UsersRole entity)
        {
            return this.Save(new List<UsersRole>() { entity });
        }

        public OperationResult<IEnumerable<UsersRole>> Save(IEnumerable<UsersRole> entities)
        {
            //Set the company ID at moment to save
            throw new NotImplementedException();
        }
    }
}
