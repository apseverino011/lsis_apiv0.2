﻿using LSIS.DATA;
using LSIS.ENTITY;
using LSIS.ENTITY.Views;
using LSIS.SERVICES.Interfaces;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace LSIS.SERVICES
{
    public partial class PeriodsServices : IBasicMethods<AccPeriods>
    {
        #region Static Properties
        public static PeriodsServices Current { get { return new PeriodsServices(); } }
        #endregion

        #region Construct

        public void Construct(AccPeriods entity)
        {
            this.Construct(new List<AccPeriods> { entity });
        }

        public void Construct(IEnumerable<AccPeriods> entities)
        {
            foreach (var per in entities)
            {
                per.PeriodDetails = AccPeriodDetailsAdapter.Current.Find(x => x.PeriodId == per.GID);
            }
        }
        #endregion

        public OperationResult<AccPeriods> LockPeriod(Guid periodId)
        {
            OperationResult<AccPeriods> result = new OperationResult<AccPeriods>();

            try
            {
                AccPeriodsAdapter.Current.ExecProcedureNotReturnData($" EXEC spLockPeriod '{periodId}'");
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<AccPeriodDetails> LockPeriodDetail(Guid periodDetailId)
        {
            OperationResult<AccPeriodDetails> result = new OperationResult<AccPeriodDetails>();

            try
            {
                AccPeriodsAdapter.Current.ExecProcedureNotReturnData($" EXEC spLockPeriodDetail '{periodDetailId}'");
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<AccPeriods> ClosePeriod(Guid periodId)
        {
            OperationResult<AccPeriods> result = new OperationResult<AccPeriods>();

            try
            {
                AccPeriodsAdapter.Current.ExecProcedureNotReturnData($" EXEC spClosePeriod '{periodId}'");
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<AccPeriodDetails> ClosePeriodDetail(Guid periodDetailId)
        {
            OperationResult<AccPeriodDetails> result = new OperationResult<AccPeriodDetails>();

            try
            {
                AccPeriodsAdapter.Current.ExecProcedureNotReturnData($" EXEC spClosePeriodDetail '{periodDetailId}'");
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<IEnumerable<Periods>> GetPerios()
        {
            OperationResult<IEnumerable<Periods>> result = new OperationResult<IEnumerable<Periods>>();

            try
            {
                using (LSISContext db = new LSISContext())
                {
                    result.Data = db.Periods.Select(c => c).ToList();
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<IEnumerable<AccPeriods>> GetAll(bool isConstruct = false)
        {
            OperationResult<IEnumerable<AccPeriods>> result = new OperationResult<IEnumerable<AccPeriods>>();

            try
            {
                result.Data = AccPeriodsAdapter.Current.GetAll();

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<AccPeriods> Find(Guid periodId, bool isConstruct = false)
        {
            OperationResult<AccPeriods> result = new OperationResult<AccPeriods>();

            try
            {
                var responce = this.Find(x => x.GID == periodId, isConstruct);

                if (responce.Success)
                {
                    result.Data = responce.Data.FirstOrDefault();
                }
                else
                {
                    throw responce.Messages;
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<IEnumerable<AccPeriods>> Find(Expression<Func<AccPeriods, bool>> condition, bool isConstruct = false)
        {
            OperationResult<IEnumerable<AccPeriods>> result = new OperationResult<IEnumerable<AccPeriods>>();

            try
            {
                result.Data = AccPeriodsAdapter.Current.Find(condition);

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<AccPeriods> Save(AccPeriods entity)
        {
            OperationResult<AccPeriods> result = new OperationResult<AccPeriods>();

            try
            {
                var response = this.Save(new List<AccPeriods> { entity });

                if (response.Success)
                {
                    result.Data = response.Data.FirstOrDefault();
                }
                else
                {
                    throw response.Messages;
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }


            return result;
        }

        public OperationResult<IEnumerable<AccPeriods>> Save(IEnumerable<AccPeriods> entities)
        {
            OperationResult<IEnumerable<AccPeriods>> result = new OperationResult<IEnumerable<AccPeriods>>() { Data = entities };

            List<AccPeriods> periods = new List<AccPeriods>();
            List<AccPeriodDetails> details = new List<AccPeriodDetails>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {

                    foreach (var item in entities)
                    {
                        periods.Add(item);

                        details.AddRange(item.PeriodDetails);
                    }

                    var _per = AccPeriodsAdapter.Current.Save(periods);
                    if (!_per.Success) { throw _per.Messages; }

                    var _det = AccPeriodDetailsAdapter.Current.Save(details);
                    if (!_det.Success) { throw _det.Messages; }

                    result.Success = true;

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    result.SetFail(ex);
                }

            }
            return result;
        }
    }
}
