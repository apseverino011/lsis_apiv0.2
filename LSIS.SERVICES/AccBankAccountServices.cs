﻿using LSIS.DATA;
using LSIS.ENTITY;
using LSIS.SERVICES.Interfaces;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.SERVICES
{
    public partial class AccBankAccountServices : IBasicMethods<AccBankAccount>
    {
        #region Static Properties
        public static AccBankAccountServices Current { get { return new AccBankAccountServices(); } }
        #endregion

        public OperationResult<IEnumerable<AccBankAccount>> GetAll(bool isConstruct = false)
        {
            OperationResult<IEnumerable<AccBankAccount>> result = new OperationResult<IEnumerable<AccBankAccount>>();

            try
            {
                result.Success = true;
                result.Data = AccBankAccountAdapter.Current.Find(x=>x.CompanyId == InternalInfo.CompanyId);
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<IEnumerable<AccBankAccount>> Find(string name, bool isConstruct = false)
        {
            OperationResult<IEnumerable<AccBankAccount>> result = new OperationResult<IEnumerable<AccBankAccount>>();

            try
            {
                string[] names = name.Split(' ');

                List<List<AccBankAccount>> baLists = new List<List<AccBankAccount>>();
                List<AccBankAccount> bankAccounts = new List<AccBankAccount>();

                foreach (string n in name.Split(' ').Where(nn => nn != string.Empty && nn.Length > 2))
                {
                    baLists.Add(AccBankAccountAdapter.Current.Find(p => p.BankAccountNumber.ToLower().Contains(n.ToLower())));
                }

                List<AccBankAccount> tempBanks = baLists.SelectMany(blp => blp).ToList();

                tempBanks.ForEach(p =>
                {
                    if (bankAccounts.Any(p2 => p2.GID.Equals(p.GID))) { return; }

                    bankAccounts.Add(p);
                });


                result.Success = true;
                result.Data = bankAccounts;
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<IEnumerable<AccBankAccount>> Find(Expression<Func<AccBankAccount, bool>> condition, bool isConstruct = false)
        {
            OperationResult<IEnumerable<AccBankAccount>> result = new OperationResult<IEnumerable<AccBankAccount>>();

            try
            {
                result.Success = true;
                result.Data = AccBankAccountAdapter.Current.Find(condition);
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }
        
        public OperationResult<IEnumerable<AccBankAccount>> Save(IEnumerable<AccBankAccount> entities)
        {
            throw new NotImplementedException();
        }
    }
}
