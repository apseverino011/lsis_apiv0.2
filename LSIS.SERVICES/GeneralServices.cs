﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LSIS.DATA;
using LSIS.ENTITY;
using LSIS.UTILITY;

namespace LSIS.SERVICES
{
    public partial class GeneralServices
    {

        #region [Static Properties]
        public static GeneralServices Current { get { return new GeneralServices(); } }
        #endregion

        public OperationResult<Company> ValidateToken(string token)
        {
            OperationResult<Company> result = new OperationResult<Company>();

            string[] tokenInf = GeneralInfo.Current.GetTokenInformation(token);

            Guid sessId = Guid.Parse(tokenInf[1]);

            var sess = SessionHistoryAdapter.Current.Find(x => x.GID == sessId).FirstOrDefault();

            InternalInfo.CompanyId = (Guid)(sess.CompanyId == Guid.Empty ? Guid.Empty : sess.CompanyId);
            InternalInfo.UserId = sess.UserId;

            if (sess != null)
            {
                result.Success = true;
                result.Data = CompanyAdapter.Current.Find(x => x.GID == sess.CompanyId).FirstOrDefault();
            }
            else
            {
                result.SetFail(new Exception("La session a expirado."));
            }

            return result;
        }

        public Guid GetCompanyID(string token)
        {
            string[] tokenInf = GeneralInfo.Current.GetTokenInformation(token);

            Guid sessId = Guid.Parse(tokenInf[1]);

            var sess = SessionHistoryAdapter.Current.Find(x => x.GID == sessId).FirstOrDefault();

            return CompanyAdapter.Current.Find(x => x.GID == sess.CompanyId).FirstOrDefault().GID;
        }


    }
}
