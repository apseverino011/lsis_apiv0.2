﻿using LSIS.DATA;
using LSIS.ENTITY;
using LSIS.SERVICES.Interfaces;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace LSIS.SERVICES
{
    public partial class PeriodStateServices : IBasicMethods<PeriodState>
    {
        #region Static Properties
        public static PeriodStateServices Current { get { return new PeriodStateServices(); } }
        #endregion

        public OperationResult<IEnumerable<PeriodState>> GetAll(bool isConstruct = false)
        {
            OperationResult<IEnumerable<PeriodState>> result = new OperationResult<IEnumerable<PeriodState>>();

            try
            {
                result.Data = PeriodStateAdapter.Current.GetAll();
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<IEnumerable<PeriodState>> Find(string periodCode, bool isConstruct = false)
        {
            return this.Find(x => x.Code == periodCode);
        }

        public OperationResult<IEnumerable<PeriodState>> Find(Expression<Func<PeriodState, bool>> condition, bool isConstruct = false)
        {
            OperationResult<IEnumerable<PeriodState>> result = new OperationResult<IEnumerable<PeriodState>>();

            try
            {
                result.Data = PeriodStateAdapter.Current.Find(condition);
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<PeriodState> Save(PeriodState entity)
        {
            OperationResult<PeriodState> result = new OperationResult<PeriodState>();

            var resp = this.Save(new List<PeriodState> { entity });

            if (resp.Success)
            {
                result.Data = resp.Data.FirstOrDefault();
            }
            else
            {
                throw resp.Messages;
            }

            return result;
        }

        public OperationResult<IEnumerable<PeriodState>> Save(IEnumerable<PeriodState> entities)
        {
            OperationResult<IEnumerable<PeriodState>> result = new OperationResult<IEnumerable<PeriodState>>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    result = PeriodStateAdapter.Current.Save(entities);

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    result.SetFail(ex);
                }
            }

            return result;
        }
    }
}
