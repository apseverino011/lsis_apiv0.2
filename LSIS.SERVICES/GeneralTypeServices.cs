﻿using LSIS.SERVICES.Interfaces;
using LSIS.ENTITY;
using LSIS.DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LSIS.UTILITY;
using System.Linq.Expressions;

namespace LSIS.SERVICES
{
    public partial class GeneralTypeServices : IBasicMethods<GeneralType>
    {
        #region [Static Properties]
        public static GeneralTypeServices Current { get { return new GeneralTypeServices(); } }
        #endregion

        #region [Static Properties]
        public void Construct(GeneralType type)
        {
            this.Construct(new List<GeneralType>() { type });
        }

        public void Construct(IEnumerable<GeneralType> types)
        {
            foreach (var gt in types)
            {
                var ggt = GeneralTypeAdapter.Current.Find(x => x.GID == gt.Status).FirstOrDefault();

                gt.StatusName = (ggt != null ? gt.Description : "Activo");

                gt.Chields = GeneralTypeAdapter.Current.Find(x => x.OwnerId == gt.GID);

                if (gt.Chields.Any())
                {
                    this.Construct(gt.Chields);
                }

            }
        }
        #endregion

        public OperationResult<bool> ValidateCode(string code, string token)
        {
            OperationResult<bool> result = new OperationResult<bool>();

            //Get company GID
            Guid companyId = InternalInfo.CompanyId;

            result.Data = this.Find(x => x.Code == code && x.CompanyId == companyId).Success;

            /*result.Data = full.Data.FirstOrDefault();

            if (result.Data == null) { result.SetFail(full.Messages); }*/

            return result;
        }

        public OperationResult<IEnumerable<GeneralType>> GetAll(bool isConstruct = false)
        {
            OperationResult<IEnumerable<GeneralType>> result = new OperationResult<IEnumerable<GeneralType>>();

            try
            {
                result.Success = true;
                result.Data = GeneralTypeAdapter.Current.GetAll();

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
                throw;
            }

            return result;
        }

        public OperationResult<IEnumerable<GeneralType>> FindChieldByParentCode(string parentCode, bool isConstruct = false)
        {
            OperationResult<IEnumerable<GeneralType>> result = new OperationResult<IEnumerable<GeneralType>>();
            try
            {
                var parent = GeneralTypeAdapter.Current.Find(x => x.Code == parentCode).FirstOrDefault();


                if (parent != null)
                {
                    result = this.Find(x => x.OwnerId == parent.GID);
                }
                else
                {
                    throw new Exception(string.Format("The code {0} do not exists", parentCode));
                }


            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;

        }

        public OperationResult<IEnumerable<GeneralType>> FindChieldByParentGID(Guid parentGid, bool isConstruct = false)
        {
            OperationResult<IEnumerable<GeneralType>> result = new OperationResult<IEnumerable<GeneralType>>();
            try
            {
                if (parentGid != null)
                {
                    result = this.Find(x => x.OwnerId == parentGid);
                }


            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<GeneralType> Find(string code, bool isConstruct = false)
        {
            OperationResult<GeneralType> result = new OperationResult<GeneralType>();

            var full = this.Find(x => x.Code == code, isConstruct);

            result.Data = full.Data.FirstOrDefault();

            if (result.Data == null) { result.SetFail(full.Messages); }

            return result;
        }

        public OperationResult<IEnumerable<GeneralType>> Find(Expression<Func<GeneralType, bool>> condition, bool isConstruct = false)
        {
            OperationResult<IEnumerable<GeneralType>> result = new OperationResult<IEnumerable<GeneralType>>();

            try
            {
                result.Success = true;
                result.Data = GeneralTypeAdapter.Current.Find(condition);
                if (result.Data.Count() <= 0) { throw new Exception("Cannt find"); }

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<IEnumerable<GeneralType>> Save(GeneralType entity)
        {
            return this.Save(new List<GeneralType>() { entity });
        }

        public OperationResult<IEnumerable<GeneralType>> Save(IEnumerable<GeneralType> entities)
        {
            OperationResult<IEnumerable<GeneralType>> result = new OperationResult<IEnumerable<GeneralType>>();

            List<GeneralType> gts = new List<GeneralType>();

            try
            {
                foreach (GeneralType generalT in entities)
                {
                    generalT.CompanyId = InternalInfo.CompanyId;

                    gts.Add(generalT);

                    if (generalT.Chields.Count() > 0)
                    {
                        gts.AddRange(generalT.Chields);
                        generalT.Chields.ForEach(res =>
                        {
                            if (res.Chields.Any())
                            {
                                res.CompanyId = InternalInfo.CompanyId;
                                res.Chields.ForEach(r => { r.CompanyId = InternalInfo.CompanyId; });
                                gts.AddRange(res.Chields);
                            }
                        });
                    }
                }

                result = GeneralTypeAdapter.Current.Save(gts);

            }
            catch (Exception ex)
            {
                result.SetFail(ex);
                throw;
            }

            return result;
        }
    }
}
