﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using LSIS.DATA;
using LSIS.ENTITY;
using LSIS.SERVICES.Interfaces;
using LSIS.UTILITY;

namespace LSIS.SERVICES
{
    public partial class CompanyServices : IBasicMethods<Company>
    {
        #region [ Stattic Properties ]
        public static CompanyServices Current { get { return new CompanyServices(); } }
        #endregion

        #region [ Consturcts methods ]

        public void Construct(Company company)
        {
            this.Construct(new List<Company>() { company });
        }

        public void Construct(IEnumerable<Company> companies)
        {
            foreach (Company com in companies)
            {
                com.Employes = CompanyEmployeAdapter.Current.Find(x => x.CompanyId == com.GID);

                com.Clients = CompanyClientAdapter.Current.Find(x => x.CompanyId == com.GID);

                com.ContactNumbers = ContactNumberServices.Current.Find(x => x.OwnerId == com.GID, true).Data.ToList();

                com.ContactPersons = ContactPersonsAdapter.Current.Find(x => x.OwnerId == com.GID);

                if (com.ContactPersons.Any())
                {
                    com.ContactPersons.ForEach(cp =>
                    {
                        cp.Contact = PartyServices.Current.Find(cp.ContactId).Data.FirstOrDefault();
                    });
                }

                com.Locations = LocationAdapter.Current.Find(x => x.OwnerId == com.GID);

                //Get modules
                var moduleIds = CompanyModuleAdapter.Current.Find(x => x.CompanyId == com.GID).Select(c => c.ModuleId);

                com.Modules = GeneralModuleAdapter.Current.Find(x => moduleIds.Contains(x.GID));
            }
        }

        #endregion

        public OperationResult<Party> GetEmployeeByCode(string code, bool isConstruct = false)
        {
            OperationResult<Party> result = new OperationResult<Party>();

            try
            {

                var emp = CompanyEmployeAdapter.Current.Find(x => x.CodeEmployee == code).FirstOrDefault();

                result.Success = true;
                result.Data = PartyAdapter.Current.Find(x => x.GID == emp.PartyGID).FirstOrDefault();

                /*if (isConstruct) { this.Construct(result.Data); }*/

            }
            catch (Exception ex)
            {
                result.SetFail(ex.InnerException);
            }

            return result;
        }

        public OperationResult<IEnumerable<Company>> Find(Guid gid, bool isConstruct = false)
        {
            return this.Find(x => x.GID == gid);
        }

        public OperationResult<IEnumerable<Company>> Find(string name, bool isConstruct = false)
        {
            return this.Find(x => x.BusinessName == name);
        }

        public OperationResult<IEnumerable<Company>> Find(Expression<Func<Company, bool>> condition, bool isConstruct = false)
        {
            OperationResult<IEnumerable<Company>> result = new OperationResult<IEnumerable<Company>>();

            try
            {
                result.Success = true;
                result.Data = CompanyAdapter.Current.Find(condition);

                if (isConstruct) { this.Construct(result.Data); }

            }
            catch (Exception ex)
            {
                result.SetFail(ex.InnerException);
            }

            return result;
        }

        public OperationResult<IEnumerable<Company>> GetAll(bool isConstruct = false)
        {
            OperationResult<IEnumerable<Company>> result = new OperationResult<IEnumerable<Company>>();

            try
            {
                result.Success = true;
                result.Data = CompanyAdapter.Current.GetAll();

                if (isConstruct) { this.Construct(result.Data); }

            }
            catch (Exception ex)
            {
                result.SetFail(ex.InnerException);
            }

            return result;
        }

        public OperationResult<Company> Save(Company entity)
        {
            OperationResult<Company> result = new OperationResult<Company>();

            try
            {
                var res = this.Save(new List<Company>() { entity });

                if (!res.Success) { throw new Exception(res.Messages.Message); }

                result.Data = res.Data.FirstOrDefault();

            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }
            return result;
        }

        public OperationResult<IEnumerable<Company>> Save(IEnumerable<Company> entities)
        {
            OperationResult<IEnumerable<Company>> result = new OperationResult<IEnumerable<Company>>();
            List<CompanyModule> compModules = new List<CompanyModule>();
            List<ContactNumber> contactNumbers = new List<ContactNumber>();
            List<ContactPerson> contactPeople = new List<ContactPerson>();
            List<Location> locations = new List<Location>();
            List<Company> companies = new List<Company>();


            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {

                    foreach (var comp in entities)
                    {
                        if (comp.Modules != null)
                        {
                            var gids = comp.Modules.Select(c => c.GID);

                            var deleteModules = CompanyModuleAdapter.Current.Find(x => x.CompanyId == comp.GID && !gids.Contains(x.ModuleId));

                            if (deleteModules.Any())
                            {

                                deleteModules.ForEach(em =>
                                {
                                    em.IsDeleted = true;
                                    em.IsActive = false;
                                });

                                compModules.AddRange(deleteModules);
                            }

                            //Create company Modules
                            comp.Modules.ForEach(mod =>
                            {
                                var modValidate = CompanyModuleAdapter.Current.Find(x => x.CompanyId == comp.GID && x.ModuleId == mod.GID).FirstOrDefault();

                                if (modValidate == null)
                                {
                                    compModules.Add(new CompanyModule()
                                    {
                                        CompanyId = comp.GID,
                                        ModuleId = mod.GID
                                    });
                                }
                            });
                        }

                        if (comp.ContactNumbers != null)
                        {
                            //Add contact numbers
                            comp.ContactNumbers.ForEach(cn =>
                            {
                                if (cn.Indx == 0)
                                {
                                    cn.OwnerId = comp.GID;
                                    contactNumbers.Add(cn);
                                }
                            });
                        }

                        if (comp.ContactPersons != null)
                        {
                            //Add contact numbers
                            comp.ContactPersons.ForEach(cp =>
                            {
                                if (cp.Indx == 0)
                                {
                                    cp.OwnerId = comp.GID;
                                    contactPeople.Add(cp);
                                }
                            });
                        }

                        if (comp.Locations != null)
                        {
                            //Locations
                            comp.Locations.ForEach(loc =>
                            {
                                loc.OwnerId = comp.GID;
                                locations.Add(loc);
                            });
                        }
                        companies.Add(comp);
                    }

                    if (!CompanyModuleAdapter.Current.Save(compModules).Success) { throw new Exception("The modules for company can't be saved"); }

                    if (!ContactNumberAdapter.Current.Save(contactNumbers).Success) { throw new Exception("The contact Number for compant can't be saved"); }

                    if (!ContactPersonsAdapter.Current.Save(contactPeople).Success) { throw new Exception("The contact Person for compant can't be saved"); }

                    if (!LocationAdapter.Current.Save(locations).Success) { throw new Exception("The locations for compant can't be saved"); }

                    if (!CompanyAdapter.Current.Save(companies).Success) { throw new Exception("The company can't be saved"); }

                    result.Data = entities;

                    scope.Complete();

                }
                catch (Exception ex)
                {
                    result.SetFail(ex.InnerException);
                    scope.Dispose();
                }
            }
            return result;
        }
    }
}
