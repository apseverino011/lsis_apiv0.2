﻿using LSIS.DATA;
using LSIS.ENTITY;
using LSIS.SERVICES.Interfaces;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace LSIS.SERVICES
{
    public partial class UserServices : IBasicMethods<User>
    {
        #region [Static Properties]
        public static UserServices Current { get { return new UserServices(); } }
        #endregion

        #region [Constructs]
        public void Construct(User user)
        {
            this.Construct(new List<User>() { user });
        }

        public void Construct(IEnumerable<User> users)
        {
            foreach (User user in users)
            {
                //user.Company = CompanyAdapter.Current.Find(x => x.GID == user.CompanyId).FirstOrDefault();

                user.Party = PartyAdapter.Current.Find(x => x.GID == user.PartyGID).FirstOrDefault();

                user.Role = UsersRoleAdapter.Current.Find(x => x.GID == user.UserRoleGID).FirstOrDefault();

                var status = GeneralTypeAdapter.Current.Find(x => x.GID == user.Status).FirstOrDefault();

                //if (status != null)
                //{
                //    user.StatusName = status.Description;
                //}
                //else
                //{
                //    user.StatusName = "Active";
                //}
            }

        }
        #endregion

        #region [ Login Methods ]

        /// <summary>
        /// This method is for validate user using user and password
        /// </summary>
        /// <param name="user">string: user</param>
        /// <param name="password">string: password</param>
        /// <param name="macAddress">string: macAddress</param>
        /// <param name="company">string: company</param>
        /// <returns>User Entity</returns>
        public OperationResult<IEnumerable<SessionHistory>> _Login(string user, string password, Guid company, string macAddress = "", string ip = "")
        {
            OperationResult<IEnumerable<SessionHistory>> result = new OperationResult<IEnumerable<SessionHistory>>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {

                try
                {
                    User validateUser = UserAdapter.Current.Find(c => c.UsrId.Equals(user) && c.IsActive).FirstOrDefault();

                    if (validateUser != null && (validateUser.ExpirationDate == null || validateUser.ExpirationDate > DateTime.Now.Date))
                    {
                        if (EncryptDecrypt.Current.DencryptText(validateUser.PssId) == password)
                        {
                            //Validate if user have other session active
                            var sess = SessionHistoryAdapter.Current.Find(x => x.UserId == validateUser.GID && x.IsActive);

                            if (sess != null && sess.Any())
                            {
                                sess.ForEach(t =>
                                {
                                    t.IsActive = false;
                                    t.EndDate = DateTime.Now;
                                    t.CompanyId = validateUser.CompanyId;
                                });

                                SessionHistoryAdapter.Current.Save(sess);
                            }

                            //Create new session
                            result = SessionHistoryAdapter.Current.Save(new SessionHistory()
                            {
                                Ip = (ip == string.Empty ? "10.0.0.1" : ip),
                                MacAddress = macAddress,
                                UserId = validateUser.GID,
                                StartDate = DateTime.Now,
                                CompanyId = validateUser.CompanyId,
                                LogDate = DateTime.Now

                            });

                        }
                        else
                        {
                            result.Success = false;
                        }

                    }
                    else
                    {
                        result.Success = false;
                    }

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    result.SetFail(ex);
                    scope.Dispose();
                }
            }
            return result;
        }

        /// <summary>
        /// That methods is used for inactive the last session actived for user
        /// </summary>
        /// <param name="sessionId">Guid: Session number</param>
        public void _LogOut(Guid sessionId)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    SessionHistory sh = SessionHistoryAdapter.Current.Find(x => x.GID == sessionId).FirstOrDefault();

                    sh.IsActive = false;
                    sh.EndDate = DateTime.Now;

                    SessionHistoryAdapter.Current.Save(sh);

                    scope.Complete();

                }
                catch (Exception ex)
                {
                    scope.Dispose();
                }
            }
        }

        /*
        /// <summary>
        /// This method is for logout the user
        /// </summary>
        /// <param name="companyGID">Guid: company</param>
        /// <param name="userGID">Guid: user</param>
        /// <param name="sessionGID">Guid: session id</param>
        /// <returns>bool</returns>
        public bool _Logout(Guid companyGID, Guid userGID, Guid sessionGID)
        {
            bool result = false;
            try
            {
                return SessionServices.Current.EnableDisableSession(sessionGID, false);
            }
            catch (Exception)
            {

                result = false;
            }

            return result;
        }

        /// <summary>
        /// This method is for Enable to Disable user using company id, user id and status
        /// </summary>
        /// <param name="companyGID">Guid: company</param>
        /// <param name="userGID">Guid: user</param>
        /// <param name="status">string: user</param>
        /// <returns>bool</returns>
        public bool EnableDisableUser(Guid companyGID, Guid userGID, string status)
        {
            bool result = false;
            try
            {
                //Entities.User validateUser = UserAdapter.Current.Find(c => c.CompanyId.Equals(companyGID) && c.UsrId.Equals(user)).FirstOrDefault();

                //if (validateUser != null)
                //{
                //}
            }
            catch (Exception)
            {

                result = true;
            }

            return result;
        }
        */

        #endregion

        public OperationResult<IEnumerable<User>> Find(string userName, bool isConstruct = false)
        {
            throw new NotImplementedException();
        }

        public OperationResult<IEnumerable<User>> FindByCompany(Guid gid, bool isConstruct = false)
        {
            return this.Find(x => x.CompanyId == gid, isConstruct);
        }

        public OperationResult<IEnumerable<User>> Find(Guid gid, bool isConstruct = false)
        {
            return this.Find(x => x.GID == gid, isConstruct);
        }

        public OperationResult<IEnumerable<User>> Find(Expression<Func<User, bool>> condition, bool isConstruct = false)
        {
            OperationResult<IEnumerable<User>> result = new OperationResult<IEnumerable<User>>();

            try
            {
                result.Success = true;
                result.Data = UserAdapter.Current.Find(condition);

                if (isConstruct) { this.Construct(result.Data); }

            }
            catch (Exception ex)
            {
                result.SetFail(ex.InnerException);

            }
            return result;
        }

        public OperationResult<IEnumerable<User>> GetAll(bool isConstruct = false)
        {
            throw new NotImplementedException();
        }

        public OperationResult<IEnumerable<User>> GetAll(string token, bool isConstruct = false)
        {
            OperationResult<IEnumerable<User>> result = new OperationResult<IEnumerable<User>>();

            try
            {
                //var compId = GeneralServices.Current.GetCompanyID(token);
                var compId = InternalInfo.CompanyId;

                result.Success = true;
                result.Data = UserAdapter.Current.Find(x => x.CompanyId == compId);

                if (isConstruct) { this.Construct(result.Data); }

            }
            catch (Exception ex)
            {
                result.SetFail(ex.InnerException);

            }
            return result;
        }

        public OperationResult<IEnumerable<User>> Save(User entity)
        {
            return this.Save(new List<User>() { entity });
        }

        public OperationResult<IEnumerable<User>> Save(IEnumerable<User> entities)
        {
            OperationResult<IEnumerable<User>> result = new OperationResult<IEnumerable<User>>();
            List<User> users = new List<User>();
            List<Party> parties = new List<Party>();
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    foreach (var user in entities)
                    {
                        user.Party.FullName = user.Party.FirstName + " " + user.Party.LastName;
                        user.FullName = user.Party.FullName;

                        parties.Add(user.Party);

                        user.PssId = EncryptDecrypt.Current.EncryptText(user.PssId);
                        user.Language = "en";
                        user.CompanyId = InternalInfo.CompanyId;

                        users.Add(user);
                    }

                    if (!this.Save(parties).Success) { throw new Exception("an error has ocurred saving party"); }

                    result = UserAdapter.Current.Save(users);

                    if (!result.Success) { throw new Exception("Han error has ocurred on user create"); }
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    result.SetFail(ex.InnerException);
                    scope.Dispose();
                }
            }

            return result;
        }

        public OperationResult<bool> Save(IEnumerable<Party> entities)
        {
            OperationResult<bool> result = new OperationResult<bool>();

            List<Party> parties = new List<Party>();
            List<ContactNumber> contactNumbers = new List<ContactNumber>();
            List<PartyIdentification> identifications = new List<PartyIdentification>();
            List<ContactPerson> contactPersons = new List<ContactPerson>();
            List<Party> partyContactPersons = new List<Party>();
            List<Location> locations = new List<Location>();

            try
            {
                foreach (Party p in entities)
                {
                    if (p.GID == null || p.GID == Guid.Empty) { p.GID = Guid.NewGuid(); }

                    parties.Add(p);

                    p.ContactNumbers.ForEach(x => { x.OwnerId = p.GID; });
                    contactNumbers.AddRange(p.ContactNumbers);

                    p.Identifications.ForEach(x => { x.Party = p.GID; });
                    identifications.AddRange(p.Identifications);

                    if (p.PersonsContact != null)
                    {
                        foreach (var par in p.PersonsContact)
                        {
                            if (par.GID == null || par.GID == Guid.Empty) { par.GID = Guid.NewGuid(); }

                            partyContactPersons.Add(par);

                            contactPersons.Add(new ContactPerson()
                            {
                                OwnerId = p.GID,
                                ContactId = par.GID
                            });
                        }
                    }

                    if (p.PartyLocations != null)
                    {
                        p.PartyLocations.ForEach(x => { x.OwnerId = p.GID; });
                        locations.AddRange(p.PartyLocations);
                    }

                }

                //Save Party
                if (!PartyAdapter.Current.Save(parties).Success) { throw new Exception("Error has occurred in Party save"); }

                //Save Contact Numbers
                if (!ContactNumberAdapter.Current.Save(contactNumbers).Success) { throw new Exception("Error has occurred in Party ContactNumbers save"); }

                //Save Identifications
                if (!PartyIdentificationAdapter.Current.Save(identifications).Success) { throw new Exception("Error has occurred in Party Identifications save"); }

                //Save Locations
                if (!LocationAdapter.Current.Save(locations).Success) { throw new Exception("Error has occurred in Party Locations save"); }

                //Save Party and Contact Person
                if (!PartyAdapter.Current.Save(partyContactPersons).Success) { throw new Exception("Error has occurred in Party Contact Person save"); }

                if (!ContactPersonsAdapter.Current.Save(contactPersons).Success) { throw new Exception("Error has occurred in Party Contact Person save"); }

            }
            catch (Exception ex)
            {
                result.SetFail(ex);
                //ErrorLog.Current.SaverError("", "Party.Find by Name", "", ex.InnerException.ToString());
            }


            return result;
        }
    }
}
