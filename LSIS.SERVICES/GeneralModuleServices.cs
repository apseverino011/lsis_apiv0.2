﻿using LSIS.DATA;
using LSIS.ENTITY;
using LSIS.SERVICES.Interfaces;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace LSIS.SERVICES
{
    public partial class GeneralModuleServices : IBasicMethods<GeneralModule>
    {
        #region [Static Properties]
        public static GeneralModuleServices Current { get { return new GeneralModuleServices(); } }
        #endregion

        #region [Construct]

        public void Construct(GeneralModule module)
        {
            this.Construct(new List<GeneralModule>() { module });
        }

        public void Construct(IEnumerable<GeneralModule> modules)
        {
            foreach (GeneralModule module in modules)
            {
                if (module.IsMain)
                {
                    module.Options = GeneralModuleAdapter.Current.Find(x => x.Owner == module.GID);

                    Construct(module.Options);

                }
                else if (module.IsSubM)
                {
                    module.Options = GeneralModuleAdapter.Current.Find(x => x.Owner == module.GID);
                }
            }
        }


        #endregion

        public OperationResult<IEnumerable<GeneralModule>> GetAllModulesCompanies(string token, bool isConstruct = false)
        {
            OperationResult<IEnumerable<GeneralModule>> result = new OperationResult<IEnumerable<GeneralModule>>();

            try
            {
                string[] tokenInf = GeneralInfo.Current.GetTokenInformation(token);

                Guid sessId = Guid.Parse(tokenInf[1]);

                var sess = SessionHistoryAdapter.Current.Find(x => x.GID == sessId).FirstOrDefault();

                Guid compId = CompanyAdapter.Current.Find(x => x.GID == sess.CompanyId).FirstOrDefault().GID;

                result.Success = true;

                var modules = CompanyModuleAdapter.Current.Find(x => x.CompanyId == compId).Select(c => c.ModuleId);

                result.Data = GeneralModuleServices.Current.Find(x => modules.Contains(x.GID), true).Data;

                if (isConstruct) { this.Construct(result.Data); }

            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<IEnumerable<GeneralModule>> GetAll(bool isConstruct = false)
        {
            throw new NotImplementedException();
        }

        public OperationResult<IEnumerable<GeneralModule>> GetAllMainModules(bool isConstruct = false)
        {
            return this.Find(x => x.IsMain == true, isConstruct);
        }

        public OperationResult<IEnumerable<GeneralModule>> GetAllSubModules(bool isConstruct = false)
        {

            return this.Find(x => x.IsSubM == true, isConstruct);
        }

        public OperationResult<IEnumerable<GeneralModule>> GetAllOptions(bool isConstruct = false)
        {
            return this.Find(x => x.Link != null, isConstruct);
        }

        public OperationResult<GeneralModule> ValidateModuleName(string name)
        {
            OperationResult<GeneralModule> result = new OperationResult<GeneralModule>();

            result.Data = this.Find(x => x.Name == name && x.IsMain).Data.FirstOrDefault();
            if (result.Data != null)
            {
                result.Success = true;
            }
            else
            {
                result.Success = false;
            }

            return result;
        }

        public OperationResult<GeneralModule> ValidateOptionName(string name, Guid owner)
        {
            OperationResult<GeneralModule> result = new OperationResult<GeneralModule>();

            result.Data = this.Find(x => x.Name == name && x.Owner == owner).Data.FirstOrDefault();
            if (result.Data != null)
            {
                result.Success = true;
            }
            else
            {
                result.Success = false;
            }

            return result;
        }

        public OperationResult<IEnumerable<GeneralModule>> FindByOptionsOwner(Guid owner, bool isConstruct = false)
        {
            return this.Find(x => x.Owner == owner, isConstruct);
        }

        public OperationResult<IEnumerable<GeneralModule>> Find(string name, bool isConstruct = false)
        {
            return this.Find(x => x.Name == name, isConstruct);
        }

        public OperationResult<IEnumerable<GeneralModule>> Find(System.Linq.Expressions.Expression<Func<GeneralModule, bool>> condition, bool isConstruct = false)
        {
            OperationResult<IEnumerable<GeneralModule>> result = new OperationResult<IEnumerable<GeneralModule>>();
            try
            {

                result.Success = true;
                result.Data = GeneralModuleAdapter.Current.Find(condition);

                if (isConstruct) { this.Construct(result.Data); }

            }
            catch (Exception ex)
            {
                result.SetFail(ex);
                throw;
            }
            return result;
        }

        public OperationResult<IEnumerable<GeneralModule>> Save(GeneralModule entity)
        {
            return this.Save(new List<GeneralModule>() { entity });
        }

        public OperationResult<IEnumerable<GeneralModule>> Save(IEnumerable<GeneralModule> entities)
        {
            OperationResult<IEnumerable<GeneralModule>> result = new OperationResult<IEnumerable<GeneralModule>>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {

                try
                {
                    var values = this.getModulesAndOptions(entities);

                    result = GeneralModuleAdapter.Current.Save(values);

                    scope.Complete();

                }
                catch (Exception ex)
                {
                    result.SetFail(ex);
                    scope.Dispose();
                }

            }

            return result;
        }

        private List<GeneralModule> getModulesAndOptions(IEnumerable<GeneralModule> generalModules)
        {
            List<GeneralModule> modules = new List<GeneralModule>();

            foreach (var mod in generalModules)
            {
                modules.Add(mod);
                if (mod.IsMain)
                {
                    foreach (var opt in mod.Options)
                    {
                        opt.Owner = mod.GID;
                        modules.Add(opt);
                        if (opt.IsSubM)
                        {
                            foreach (var subOpt in opt.Options)
                            {
                                subOpt.Owner = opt.GID;
                                modules.Add(subOpt);
                            }
                        }
                    }
                }
            }

            return modules;
        }
    }
}
