﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using LSIS.DATA;
using LSIS.ENTITY;
using LSIS.SERVICES.Interfaces;
using LSIS.UTILITY;

namespace LSIS.SERVICES
{
    public partial class LoanRequestServices : IBasicMethods<LoanRequest>
    {
        #region [Static Properties]
        public static LoanRequestServices Current { get { return new LoanRequestServices(); } }
        #endregion


        #region [ Constructs]
        public void Construct(LoanRequest request)
        {
            this.Construct(new List<LoanRequest>() { request });
        }

        public void Construct(IEnumerable<LoanRequest> requests)
        {
            foreach (LoanRequest request in requests)
            {
                request.Warranties = WarrantyAdapter.Current.Find(x => x.RequestGID == request.GID);
            }
        }

        #endregion

        public OperationResult<IEnumerable<LoanRequest>> GetAll(Guid companyId, bool isConstruct = false)
        {
            OperationResult<IEnumerable<LoanRequest>> result = new OperationResult<IEnumerable<LoanRequest>>();

            try
            {
                result.Success = true;
                result.Data = LoanRequestAdapter.Current.Find(x => x.CompanyId == companyId);

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }
            return result;
        }

        public OperationResult<IEnumerable<LoanRequest>> GetAll(bool isConstruct = false)
        {
            OperationResult<IEnumerable<LoanRequest>> result = new OperationResult<IEnumerable<LoanRequest>>();

            try
            {
                result.Success = true;
                result.Data = LoanRequestAdapter.Current.GetAll();

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }
            return result;
        }

        public OperationResult<IEnumerable<LoanRequest>> Find(string number, bool isConstruct = false)
        {
            return this.Find(x => x.Number == number);
        }

        public OperationResult<IEnumerable<LoanRequest>> Find(Expression<Func<LoanRequest, bool>> condition, bool isConstruct = false)
        {
            OperationResult<IEnumerable<LoanRequest>> result = new OperationResult<IEnumerable<LoanRequest>>();

            try
            {
                result.Success = true;
                result.Data = LoanRequestAdapter.Current.Find(condition);

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
                throw;
            }

            return result;
        }

        public OperationResult<IEnumerable<LoanRequest>> Save(LoanRequest entity)
        {
            return this.Save(new List<LoanRequest>() { entity });
        }

        public OperationResult<IEnumerable<LoanRequest>> Save(IEnumerable<LoanRequest> entities)
        {
            OperationResult<IEnumerable<LoanRequest>> result = new OperationResult<IEnumerable<LoanRequest>>();

            List<Party> partys = new List<Party>();
            List<LoanRequest> loans = new List<LoanRequest>();
            List<Warranty> warranties = new List<Warranty>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    foreach (LoanRequest loan in entities)
                    {
                        //Validate GID value
                        if (loan.GID == null || loan.GID == Guid.Empty) { loan.GID = Guid.NewGuid(); }
                        loan.CompanyId = InternalInfo.CompanyId;

                        partys.Add(loan.Party);

                        loans.Add(loan);

                        //Update Loans Id by Warranty
                        loan.Warranties.ForEach(x => { if (x.Indx == 0) { x.RequestGID = loan.GID; } });

                        warranties.AddRange(loan.Warranties);

                    }

                    //Saved Party
                    if (!PartyServices.Current.Save(partys).Success) { throw new Exception("Party can't be saved"); }

                    //Saved loans
                    if (!LoanRequestAdapter.Current.Save(loans).Success) { throw new Exception("The loans can't be saved."); }
                    //Saved Warranties
                    if (!WarrantyAdapter.Current.Save(warranties).Success) { throw new Exception("The warranties can't be saved."); }

                    result.Success = true;

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    result.SetFail(ex);
                    scope.Dispose();
                }
            }
            return result;
        }
    }
}
