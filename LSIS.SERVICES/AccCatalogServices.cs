﻿using LSIS.DATA;
using LSIS.ENTITY;
using LSIS.ENTITY.Views;
using LSIS.SERVICES.Interfaces;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace LSIS.SERVICES
{
    public partial class AccCatalogServices : IBasicMethods<AccCatalog>
    {
        #region static property
        public static AccCatalogServices Current { get { return new AccCatalogServices(); } }
        #endregion

        public void Construct(AccCatalog catalog)
        {
            this.Construct(new List<AccCatalog>() { catalog });
        }

        public void Construct(IEnumerable<AccCatalog> catalogs)
        {
            foreach (AccCatalog cat in catalogs)
            {
                cat.ParentAccount = AccCatalogAdapter.Current.Find(x => x.GID == cat.ParentAccountId).FirstOrDefault();

                if (cat.ParentAccountId == Guid.Empty)
                {
                    cat.SubAccounts = AccCatalogAdapter.Current.Find(x => x.ParentAccountId == cat.GID);
                }

            }
        }

        public OperationResult<IEnumerable<ParentAccounts>> GetParents()
        {
            OperationResult<IEnumerable<ParentAccounts>> result = new OperationResult<IEnumerable<ParentAccounts>>();

            try
            {
                using (LSISContext db = new LSISContext())
                {
                    result.Data = db.ParentAccounts.Select(c => c).ToList();
                }

            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<IEnumerable<AccCatalog>> GetAll(bool isConstruct = false)
        {
            OperationResult<IEnumerable<AccCatalog>> result = new OperationResult<IEnumerable<AccCatalog>>();

            try
            {
                result.Data = AccCatalogAdapter.Current.Find(x => x.CompanyId == InternalInfo.CompanyId);

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<AccCatalog> Find(Guid gid, bool isConstruct = false)
        {
            OperationResult<AccCatalog> result = new OperationResult<AccCatalog>();

            try
            {
                var res = this.Find(x => x.GID == gid, isConstruct);

                if (!res.Success) { throw res.Messages; }

                result.Data = res.Data.FirstOrDefault();
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<IEnumerable<AccCatalog>> Find(Expression<Func<AccCatalog, bool>> condition, bool isConstruct = false)
        {
            OperationResult<IEnumerable<AccCatalog>> result = new OperationResult<IEnumerable<AccCatalog>>();

            try
            {
                result.Data = AccCatalogAdapter.Current.Find(condition).Where(x => x.CompanyId == InternalInfo.CompanyId);

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<IEnumerable<AccCatalog>> Save(AccCatalog entity)
        {
            return this.Save(new List<AccCatalog>() { entity });
        }

        public OperationResult<IEnumerable<AccCatalog>> Save(IEnumerable<AccCatalog> entities)
        {
            OperationResult<IEnumerable<AccCatalog>> result = new OperationResult<IEnumerable<AccCatalog>>();
            List<AccCatalog> Accounts = new List<AccCatalog>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    Accounts.AddRange(entities);

                    entities.ToList().ForEach(res =>
                    {
                        if (res.SubAccounts.Any())
                        {
                            Accounts.AddRange(res.SubAccounts);
                        }

                    });

                    Accounts.ForEach(res => { res.CompanyId = InternalInfo.CompanyId; });

                    result.Success = true;
                    var accounts = AccCatalogAdapter.Current.Save(Accounts);
                    if (!accounts.Success) { throw accounts.Messages; }

                    //Increise the account next number


                    scope.Complete();
                }
                catch (Exception ex)
                {
                    result.SetFail(ex);
                    scope.Dispose();
                }
            }

            return result;
        }
    }
}
