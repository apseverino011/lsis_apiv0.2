﻿using LSIS.DATA;
using LSIS.ENTITY;
using LSIS.SERVICES.Interfaces;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace LSIS.SERVICES
{
    public partial class AccGroupServices : IBasicMethods<AccGroup>
    {
        #region Static Properties
        public static AccGroupServices Current { get { return new AccGroupServices(); } }
        #endregion

        public void Construct(AccGroup group)
        {
            this.Construct(new List<AccGroup>() { group });
        }

        public void Construct(IEnumerable<AccGroup> groups)
        {
            foreach (var group in groups)
            {
                //group.AccountTypeName = GeneralTypeAdapter.Current.Find(x => x.GID == group.AccTypeId).FirstOrDefault().Description;

                //group.AccountOriginName = GeneralTypeAdapter.Current.Find(x => x.GID == group.AccOriginId).FirstOrDefault().Description;
            }
        }

        public OperationResult<string> GetNextAccNumber(Guid accGroupId)
        {
            OperationResult<string> result = new OperationResult<string>();

            try
            {
                var group = AccGroupAdapter.Current.Find(x => x.GID == accGroupId).FirstOrDefault();

                string accNumber = string.Concat("000", group.NextAccNumber.ToString());

                result.Data = accNumber.Substring(accNumber.Length - 3, 3);

                group.NextAccNumber++;

                AccGroupAdapter.Current.Save(group);
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<string> GetNextSubAccNumber(Guid accGroupId)
        {
            OperationResult<string> result = new OperationResult<string>();

            try
            {
                var group = AccGroupAdapter.Current.Find(x => x.GID == accGroupId).FirstOrDefault();

                string accNumber = string.Concat("0000", group.NextSubAccNumber.ToString());

                result.Data = accNumber.Substring(accNumber.Length - 4, 4);

                group.NextSubAccNumber++;

                AccGroupAdapter.Current.Save(group);

            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<bool> SetNextSubAccNumber(Guid accGroupId)
        {
            OperationResult<bool> result = new OperationResult<bool>();

            try
            {
                var group = AccGroupAdapter.Current.Find(x => x.GID == accGroupId).FirstOrDefault();

                group.NextSubAccNumber--;

                result.Data = AccGroupAdapter.Current.Save(group).Success;

            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<IEnumerable<AccGroup>> GetAll(bool isConstruct = false)
        {
            OperationResult<IEnumerable<AccGroup>> result = new OperationResult<IEnumerable<AccGroup>>();

            try
            {
                result.Data = AccGroupAdapter.Current.Find(c => c.CompanyId == InternalInfo.CompanyId);

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<IEnumerable<AccGroup>> Find(Guid gid, bool isConstruct = false)
        {
            return this.Find(x => x.GID == gid, isConstruct);
        }

        public OperationResult<IEnumerable<AccGroup>> Find(Expression<Func<AccGroup, bool>> condition, bool isConstruct = false)
        {
            OperationResult<IEnumerable<AccGroup>> result = new OperationResult<IEnumerable<AccGroup>>();

            try
            {
                result.Data = AccGroupAdapter.Current.Find(condition).Where(x => x.CompanyId == InternalInfo.CompanyId);

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<IEnumerable<AccGroup>> Save(AccGroup entity)
        {
            return this.Save(new List<AccGroup>() { entity });
        }

        public OperationResult<IEnumerable<AccGroup>> Save(IEnumerable<AccGroup> entities)
        {
            OperationResult<IEnumerable<AccGroup>> result = new OperationResult<IEnumerable<AccGroup>>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    //Set company id in group to be saved
                    entities.ToList().ForEach(r => { r.CompanyId = InternalInfo.CompanyId; });

                    //Saved
                    result = AccGroupAdapter.Current.Save(entities);
                    scope.Complete();
                }
                catch (Exception ex)
                {
                    result.SetFail(ex);
                    scope.Dispose();
                }
            }
            return result;
        }
    }
}
