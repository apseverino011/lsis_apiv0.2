﻿using LSIS.DATA;
using LSIS.ENTITY;
using LSIS.SERVICES.Interfaces;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.SERVICES
{
    public partial class ContactNumberServices : IBasicMethods<ContactNumber>
    {

        #region Static Properties
        public static ContactNumberServices Current { get { return new ContactNumberServices(); } }
        #endregion

        public void Construct(ContactNumber contactNumber)
        {
            this.Construct(new List<ContactNumber>() { contactNumber });
        }

        public void Construct(IEnumerable<ContactNumber> contactNumbers)
        {
            foreach (var cn in contactNumbers)
            {
                //cn.ContactTypeName = GeneralTypeAdapter.Current.Find(x=> x.GID == cn.ContactType).FirstOrDefault().Description;
            }
        }

        public OperationResult<IEnumerable<ContactNumber>> Find(Expression<Func<ContactNumber, bool>> condition, bool isConstruct = false)
        {
            OperationResult<IEnumerable<ContactNumber>> result = new OperationResult<IEnumerable<ContactNumber>>();

            try
            {
                result.Success = true;
                result.Data = ContactNumberAdapter.Current.Find(condition);

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<IEnumerable<ContactNumber>> GetAll(bool isConstruct = false)
        {
            throw new NotImplementedException();
        }

        public OperationResult<IEnumerable<ContactNumber>> Save(IEnumerable<ContactNumber> entities)
        {
            throw new NotImplementedException();
        }
    }
}
