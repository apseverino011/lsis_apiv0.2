﻿using LSIS.DATA;
using LSIS.ENTITY;
using LSIS.SERVICES.Interfaces;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace LSIS.SERVICES
{
    public partial class WarrantyServices : IBasicMethods<Warranty>
    {
        #region [Static properties]
        public static WarrantyServices Current { get { return new WarrantyServices(); } }
        #endregion

        #region [Construct Methods]
        public void Construct(Warranty warranty)
        {
            this.Construct(new List<Warranty>() { warranty });
        }

        public void Construct(IEnumerable<Warranty> warranties)
        {
            foreach (Warranty warranty in warranties)
            {
                string code = GeneralTypeAdapter.Current.Find(x => x.GID == warranty.WarrantyTypeGID).FirstOrDefault().Code;

                //Create warranty by warrantyType Code
                switch (code)
                {
                    case "VEH":
                        warranty.Vehicle = VehicleAdapter.Current.Find(x => x.GID == warranty.WarrantyGID).FirstOrDefault();
                        break;

                    case "PRO":
                        warranty.Property = PropertyAdapter.Current.Find(x => x.GID == warranty.WarrantyGID).FirstOrDefault();
                        break;

                    case "PAR":
                        warranty.Person = PartyAdapter.Current.Find(x => x.GID == warranty.WarrantyGID).FirstOrDefault();
                        break;
                    default:
                        break;
                }
            }
        }


        #endregion

        public OperationResult<IEnumerable<Warranty>> Find(Guid requiestId, bool isConstruct = false)
        {
            return this.Find(x => x.RequestGID == requiestId, isConstruct);
        }

        public OperationResult<IEnumerable<Warranty>> Find(Expression<Func<Warranty, bool>> condition, bool isConstruct = false)
        {
            OperationResult<IEnumerable<Warranty>> result = new OperationResult<IEnumerable<Warranty>>();

            try
            {
                result.Success = true;

                result.Data = WarrantyAdapter.Current.Find(condition);

                if (isConstruct) { this.Construct(result.Data); }

            }
            catch (Exception ex)
            {
                result.SetFail(ex);
                throw;
            }


            return result;
        }

        public OperationResult<IEnumerable<Warranty>> GetAll(bool isConstruct = false)
        {
            OperationResult<IEnumerable<Warranty>> result = new OperationResult<IEnumerable<Warranty>>();

            try
            {
                result.Success = true;

                result.Data = WarrantyAdapter.Current.GetAll();

                if (isConstruct) { this.Construct(result.Data); }

            }
            catch (Exception ex)
            {
                result.SetFail(ex);
                throw;
            }


            return result;
        }

        public OperationResult<IEnumerable<Warranty>> Save(Warranty entity)
        {
            return this.Save(new List<Warranty>() { entity });
        }

        public OperationResult<IEnumerable<Warranty>> Save(IEnumerable<Warranty> entities)
        {
            OperationResult<IEnumerable<Warranty>> result = new OperationResult<IEnumerable<Warranty>>();

            List<Party> parties = new List<Party>();
            List<Property> properties = new List<Property>();
            List<Vehicle> vehicles = new List<Vehicle>();
            List<Warranty> warranties = new List<Warranty>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    foreach (Warranty warranty in entities)
                    {
                        string code = GeneralTypeAdapter.Current.Find(x => x.GID == warranty.WarrantyTypeGID).FirstOrDefault().Code;

                        //Create warranty by warrantyType Code
                        switch (code)
                        {
                            case "VEH":

                                //Add warraty Id
                                if (warranty.Vehicle.GID == null || warranty.Vehicle.GID == Guid.Empty) { warranty.Vehicle.GID = Guid.NewGuid(); }

                                vehicles.Add(warranty.Vehicle);

                                //Update warratyId
                                warranty.WarrantyGID = warranty.Vehicle.GID;

                                warranties.Add(warranty);

                                break;

                            case "PRO":

                                //Add warraty Id
                                if (warranty.Property.GID == null || warranty.Property.GID == Guid.Empty) { warranty.Property.GID = Guid.NewGuid(); }

                                properties.Add(warranty.Property);

                                //Update warratyId
                                warranty.WarrantyGID = warranty.Property.GID;

                                warranties.Add(warranty);

                                break;

                            case "PAR":

                                //Add warraty Id
                                if (warranty.Person.GID == null || warranty.Person.GID == Guid.Empty) { warranty.Person.GID = Guid.NewGuid(); }

                                parties.Add(warranty.Person);

                                //Update warratyId
                                warranty.WarrantyGID = warranty.Person.GID;

                                warranties.Add(warranty);

                                break;
                            default:
                                break;
                        }
                    }

                    //Saved Warranties
                    if (!WarrantyAdapter.Current.Save(warranties).Success) { throw new Exception("The warranties can't be saved"); }
                    //Saved Person
                    if (!PartyAdapter.Current.Save(parties).Success) { throw new Exception("The Persons can't be saved"); }
                    //Saved Properties
                    if (!PropertyAdapter.Current.Save(properties).Success) { throw new Exception("The Properties can't be saved"); }
                    //Saved Vehicles
                    if (!VehicleAdapter.Current.Save(vehicles).Success) { throw new Exception("The Vehicles can't be saved"); }

                    result.Success = true;

                    scope.Complete();

                }
                catch (Exception ex)
                {
                    result.SetFail(ex);
                    scope.Dispose();
                    throw;
                }
            }

            return result;
        }


    }
}

