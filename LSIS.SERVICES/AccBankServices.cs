﻿using LSIS.DATA;
using LSIS.ENTITY;
using LSIS.SERVICES.Interfaces;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace LSIS.SERVICES
{
    public partial class AccBankServices : IBasicMethods<AccBank>
    {
        #region static properties
        public static AccBankServices Current { get { return new AccBankServices(); } }
        #endregion

        public void Construct(AccBank bank)
        {
            this.Construct(new List<AccBank>() { bank });
        }

        public void Construct(IEnumerable<AccBank> banks)
        {
            foreach (var bank in banks)
            {
                bank.Accounts = AccBankAccountAdapter.Current.Find(x => x.BankId == bank.GID);
            }

        }

        public OperationResult<IEnumerable<AccBank>> GetAll(bool isConstruct = false)
        {
            OperationResult<IEnumerable<AccBank>> result = new OperationResult<IEnumerable<AccBank>>();

            try
            {
                result.Success = true;
                result.Data = AccBankAdapter.Current.Find(x => x.CompanyId == InternalInfo.CompanyId);

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<AccBank> Find(Guid gid, bool isConstruct = false)
        {
            OperationResult<AccBank> result = new OperationResult<AccBank>();

            result.Data = this.Find(x => x.GID == gid, isConstruct).Data.FirstOrDefault();

            return result;
        }

        public OperationResult<IEnumerable<AccBank>> Find(string name, bool isConstruct = false)
        {
            OperationResult<IEnumerable<AccBank>> result = new OperationResult<IEnumerable<AccBank>>();

            try
            {
                string[] names = name.Split(' ');

                List<List<AccBank>> bLists = new List<List<AccBank>>();
                List<AccBank> banks = new List<AccBank>();

                foreach (string n in name.Split(' ').Where(nn => nn != string.Empty && nn.Length > 2))
                {
                    bLists.Add(AccBankAdapter.Current.Find(p => p.Name.ToLower().Contains(n.ToLower())));
                }

                List<AccBank> tempBanks = bLists.SelectMany(blp => blp).ToList();

                tempBanks.ForEach(p =>
                {
                    if (banks.Any(p2 => p2.GID.Equals(p.GID))) { return; }

                    banks.Add(p);
                });


                result.Success = true;
                result.Data = banks;

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex.InnerException);
                //ErrorLog.Current.SaverError("", "Party.Find by Name", "", ex.InnerException.ToString());
            }

            return result;

        }

        public OperationResult<IEnumerable<AccBank>> Find(Expression<Func<AccBank, bool>> condition, bool isConstruct = false)
        {
            OperationResult<IEnumerable<AccBank>> result = new OperationResult<IEnumerable<AccBank>>();

            try
            {
                result.Success = true;
                result.Data = AccBankAdapter.Current.Find(condition);

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        public OperationResult<AccBank> Save(AccBank entitiy)
        {
            OperationResult<AccBank> result = new OperationResult<AccBank>();

            var res = this.Save(new List<AccBank>() { entitiy });

            if (!res.Success) { throw new Exception(res.Messages.Message); }

            result.Data = res.Data.FirstOrDefault();

            return result;
        }

        public OperationResult<IEnumerable<AccBank>> Save(IEnumerable<AccBank> entities)
        {
            OperationResult<IEnumerable<AccBank>> result = new OperationResult<IEnumerable<AccBank>>();

            List<AccBank> banks = new List<AccBank>();
            List<AccBankAccount> bankAccounts = new List<AccBankAccount>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    foreach (AccBank bank in entities)
                    {
                        banks.Add(bank);

                        if (bank.Accounts.Any()) { bankAccounts.AddRange(bank.Accounts); }
                    }

                    var abc = AccBankAccountAdapter.Current.Save(bankAccounts);
                    if (!abc.Success) { throw abc.Messages; }

                    var ab = AccBankAdapter.Current.Save(banks);
                    if (!ab.Success) { throw ab.Messages; }

                    scope.Complete();
                }
                catch (Exception ex)
                {
                    result.SetFail(ex);
                    scope.Dispose();
                }
            }

            return result;
        }
    }
}
