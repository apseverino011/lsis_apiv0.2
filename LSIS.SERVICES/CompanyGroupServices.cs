﻿using LSIS.DATA;
using LSIS.ENTITY;
using LSIS.SERVICES.Interfaces;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace LSIS.SERVICES
{
    public partial class CompanyGroupServices : IBasicMethods<CompanyGroup>
    {

        #region [ Static Properties ]
        public static CompanyGroupServices Current { get { return new CompanyGroupServices(); } }
        #endregion

        #region [ Consturcts methods ]
        public void Construct(CompanyGroup group)
        {
            this.Construct(new List<CompanyGroup>() { group });
        }

        public void Construct(IEnumerable<CompanyGroup> groups)
        {
            foreach (CompanyGroup group in groups)
            {
                group.Companies = CompanyAdapter.Current.Find(x => x.GroupGID == group.GID);
            }
        }
        #endregion

        public OperationResult<IEnumerable<CompanyGroup>> Find(Guid gid, bool isConstruct = false)
        {
            return this.Find(x => x.GID == gid, isConstruct);
        }

        public OperationResult<IEnumerable<CompanyGroup>> Find(Expression<Func<CompanyGroup, bool>> condition, bool isConstruct = false)
        {
            OperationResult<IEnumerable<CompanyGroup>> result = new OperationResult<IEnumerable<CompanyGroup>>();

            try
            {
                result.Success = true;
                result.Data = CompanyGroupAdapter.Current.Find(condition);

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex.InnerException);

                //ErrorLog.Current.SaverError("", "DatosBancarion.Find", "", ex.InnerException.ToString());
            }
            return result;
        }

        public OperationResult<IEnumerable<CompanyGroup>> GetAll(bool isConstruct = false)
        {
            OperationResult<IEnumerable<CompanyGroup>> result = new OperationResult<IEnumerable<CompanyGroup>>();

            try
            {
                result.Success = true;
                result.Data = CompanyGroupAdapter.Current.GetAll();

                if (isConstruct) { this.Construct(result.Data); }
            }
            catch (Exception ex)
            {
                result.SetFail(ex.InnerException);

                //ErrorLog.Current.SaverError("", "DatosBancarion.Find", "", ex.InnerException.ToString());
            }
            return result;
        }

        public OperationResult<IEnumerable<CompanyGroup>> Save(CompanyGroup entity)
        {
            return this.Save(new List<CompanyGroup>() { entity });
        }

        public OperationResult<IEnumerable<CompanyGroup>> Save(IEnumerable<CompanyGroup> entities)
        {
            OperationResult<IEnumerable<CompanyGroup>> result = new OperationResult<IEnumerable<CompanyGroup>>();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    result = CompanyGroupServices.Current.Save(entities);

                    scope.Complete();

                }
                catch (Exception ex)
                {
                    scope.Dispose();
                    result.SetFail(ex.InnerException);

                    //RegistroErroresServices.Current.SaverError("", "CompanyGroup.Save", "", ex.InnerException.ToString());
                }
            }

            return result;
        }

    }
}
