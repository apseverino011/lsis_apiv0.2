﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.UTILITY
{
    public static class ObjectUtility
    {
        public static T FillProperties<T>(T obj, bool changeId = false) where T : new()
        {
            var newTobj = new T();
            Type nObjType = newTobj.GetType();
            Type mObjType = obj.GetType();

            System.Reflection.PropertyInfo[] tObjPropertiesInfo = nObjType.GetProperties();
            System.Reflection.PropertyInfo[] mObjPropertiesInfo = mObjType.GetProperties();

            foreach (System.Reflection.PropertyInfo info in mObjPropertiesInfo)
            {
                var tp = tObjPropertiesInfo.FirstOrDefault(x => x.Name == info.Name);

                if (tp != null)
                {
                    if (tp.PropertyType.FullName == info.PropertyType.FullName)
                    {
                        tp.SetValue(newTobj, info.GetValue(obj, null), null);
                    }
                    else if (info.PropertyType.BaseType.Name.ToLower() == "enum")
                    {
                        tp.SetValue(newTobj, info.GetValue(obj, null).ToString(), null);
                    }
                    else if (tp.PropertyType.BaseType.Name.ToLower() == "enum")
                    {
                        tp.SetValue(newTobj, Enum.Parse(tp.PropertyType, info.GetValue(obj, null).ToString()));
                    }
                }
            }

            return newTobj;
        }

        public static void Copy<T>(this object thisObj, T obj, bool changeId = false)
        {
            Type mObjType = thisObj.GetType();
            Type nObjType = obj.GetType();

            System.Reflection.PropertyInfo[] tObjPropertiesInfo = nObjType.GetProperties();
            System.Reflection.PropertyInfo[] mObjPropertiesInfo = mObjType.GetProperties();

            foreach (System.Reflection.PropertyInfo info in mObjPropertiesInfo)
            {
                var tp = tObjPropertiesInfo.FirstOrDefault(x => x.Name == info.Name);

                if (tp != null)
                {
                    if ((info.GetValue(obj, null) == null && tp.GetValue(thisObj, null) == null) || tp.GetSetMethod(true) == null) { continue; }
                    var obj1 = info.GetValue(obj, null);
                    var obj2 = tp.GetValue(thisObj, null);

                    if (obj1 == null || !obj1.Equals(obj2))
                    {
                        tp.SetValue(thisObj, obj1, null);
                    }
                }
            }
        }

        public static int DateToInt(this DateTime date)
        {
            string strDate = string.Format("{0}{1}{2}", date.Year, date.Month, date.Day);
            return int.Parse(strDate);
        }

        private static object GetPropertyValue(object obj, string propertyName)
        {
            return obj.GetType().GetProperties().FirstOrDefault(p => p.Name == propertyName).GetValue(obj, null);
        }

        public static string GetEnumDescription(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        public static string GetTableName<T>(this T obj) where T : class, new()
        {
            string tableName = typeof(T).Name;
        
            var customAttributes = typeof(T).GetTypeInfo().GetCustomAttributes<System.ComponentModel.DataAnnotations.Schema.TableAttribute>();
            if (customAttributes.Count() > 0)
            {
                tableName = customAttributes.First().Name;
            }

            return tableName;
        }
    }
}
