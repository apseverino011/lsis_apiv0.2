﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LSIS.ENTITY;
using LSIS.ENTITY.DTOs;

namespace LSIS.UTILITY
{
    public partial class CustomErrors
    {

        //General Error
        public static Error GR01 { get { return new Error() { }; } }

        //Period Error
        public static Error PE01 { get { return new Error() { }; } }
        public static Error PE02 { get { return new Error() { }; } }
        public static Error PE03 { get { return new Error() { }; } }
        public static Error PE04 { get { return new Error() { }; } }
    }
}
