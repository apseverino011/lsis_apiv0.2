﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.UTILITY
{
    public static class InternalInfo
    {
        public static Guid CompanyId { get; set; }

        public static Nullable<Guid> UserId { get; set; }

    }
}
