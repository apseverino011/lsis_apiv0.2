﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.UTILITY
{
    public partial class OperationResult<T> //where T : class
    {

        #region [ Properties ]

        public bool Success { get; set; }

        public Exception Messages { get; set; }

        public T Data { get; set; }

        #endregion

        public OperationResult()
        {
            Success = true;
        }

        public void SetFail(Exception messages)
        {
            Success = false;
            Messages = messages;
        }


    }
}
