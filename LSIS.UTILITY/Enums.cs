﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.UTILITY
{
    public static class Enums
    {
        public enum WarratyType
        {
            [Description("VEH")]
            Vehicle,
            [Description("PAR")]
            Person,
            [Description("PRO")]
            Properties
        }
    }
}
