﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Globalization;

namespace LSIS.UTILITY
{
    public partial class GeneralInfo
    {
        #region [Static Properties]
        public static GeneralInfo Current { get { return new GeneralInfo(); } }
        #endregion

        public string GenerateToken(string usrId, string sessionId)
        {
            string unitValues = string.Concat(usrId, "@", sessionId);

            return EncryptDecrypt.Current.EncryptText(unitValues);
        }

        public string[] GetTokenInformation(string token)
        {
            string tokenValue = EncryptDecrypt.Current.DencryptText(token);

            return tokenValue.Split('@');
        }

    }
}