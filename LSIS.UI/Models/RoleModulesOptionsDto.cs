﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LSIS.UI.Models
{
    public class RoleModulesOptionsDto
    {
        public Guid GID { get; set; }
        public string Role { get; set; }

        public string Name { get; set; }

        public string Link { get; set; }

        public bool _select { get; set; }
        public bool _update { get; set; }
        public bool _delete { get; set; }
        public bool _insert { get; set; }

        public Guid? Owner { get; set; }
        public bool IsMain { get; set; }
        public bool IsSubM { get; set; }

        public int Sort { get; set; }
        
        //SubOptions
        public List<RoleModulesOptionsDto> Options { get; set; }

    }
}