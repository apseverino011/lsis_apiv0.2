﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LSIS.UI.Models
{
    public class RoleModulesDto
    {
        public string Role { get; set; }

        public string ModuleName { get; set; }

        public List<RoleModulesOptionsDto> Options { get; set; }
    }
}