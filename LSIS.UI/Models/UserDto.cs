﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LSIS.UI.Models
{
    public class UserDto
    {

        public string Token { get; set; }

        public string UserName { get; set; }

        public int TimeSession { get; set; }

        public string Language { get; set; }

        public List<RoleModulesOptionsDto> Modules { get; set; }
    }
}