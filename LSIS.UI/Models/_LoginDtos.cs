﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LSIS.UI.Models
{
    public class _LoginDtos
    {
        public string UserName { get; set; }

        public string Password { get; set; }

    }
}