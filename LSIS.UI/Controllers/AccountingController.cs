﻿using LSIS.ENTITY;
using LSIS.ENTITY.Views;
using LSIS.SERVICES;
using LSIS.UTILITY;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LSIS.UI.Controllers
{
    public class AccountingController : ApiController
    {
        #region Groups Region

        [HttpGet]
        [Route("~/LSIS/group/GetNextAccNumber")]
        [SwaggerResponse(HttpStatusCode.OK, "", typeof(OperationResult<string>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "", typeof(string))]
        public IHttpActionResult GetNextAccNumber(Guid groupId)
        {
            IHttpActionResult result;

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    var resp = AccGroupServices.Current.GetNextAccNumber(groupId);

                    if (!resp.Data.Any()) { throw new Exception("Not rows found"); }

                    result = Ok(resp);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result = BadRequest(ex.Message);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/group/GetNextSubAccNumber")]
        public OperationResult<string> GetNextSubAccNumber(Guid groupId)
        {
            OperationResult<string> result = new OperationResult<string>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = AccGroupServices.Current.GetNextSubAccNumber(groupId);

                    if (!result.Data.Any()) { throw new Exception("Not rows found"); }
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpPut]
        [Route("~/LSIS/group/SetNextSubAccNumber/{groupId}")]
        public OperationResult<bool> SetNextSubAccNumber(Guid groupId)
        {
            OperationResult<bool> result = new OperationResult<bool>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = AccGroupServices.Current.SetNextSubAccNumber(groupId);

                    if (!result.Data) { throw new Exception("Can't be updated"); }
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }


        [HttpGet]
        [Route("~/LSIS/group/GetAll")]
        public OperationResult<IEnumerable<AccGroup>> GetallGroups(bool isConstruct = false)
        {
            OperationResult<IEnumerable<AccGroup>> result = new OperationResult<IEnumerable<AccGroup>>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = AccGroupServices.Current.GetAll(isConstruct);

                    if (!result.Data.Any()) { throw new Exception("Not rows found"); }
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/group/FindById")]
        public OperationResult<AccGroup> FindGroupById(Guid gid, bool isConstruct = false)
        {
            OperationResult<AccGroup> result = new OperationResult<AccGroup>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result.Data = AccGroupServices.Current.Find(gid, isConstruct).Data.FirstOrDefault();

                    if (result.Data == null) { throw new Exception("Not rows found"); }
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpPost]
        [Route("~/LSIS/group/Save")]
        public OperationResult<IEnumerable<AccGroup>> Save(AccGroup group)
        {
            OperationResult<IEnumerable<AccGroup>> result = new OperationResult<IEnumerable<AccGroup>>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = AccGroupServices.Current.Save(group);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        #endregion

        #region Classes Region

        [HttpGet]
        [Route("~/LSIS/class/GetAll")]
        public OperationResult<IEnumerable<AccClass>> GetallClasses(bool isConstruct = false)
        {
            OperationResult<IEnumerable<AccClass>> result = new OperationResult<IEnumerable<AccClass>>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = AccClassServices.Current.GetAll(isConstruct);

                    if (!result.Data.Any()) { throw new Exception("Not rows found"); }
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/class/FindById")]
        public OperationResult<AccClass> FindClassesById(Guid gid, bool isConstruct = false)
        {
            OperationResult<AccClass> result = new OperationResult<AccClass>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result.Data = AccClassServices.Current.Find(gid, isConstruct).Data;

                    if (result.Data == null) { throw new Exception("Not rows found"); }
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/class/FindClassesByGroupId")]
        public OperationResult<IEnumerable<AccClass>> FindClassesByGroup(Guid groupId, bool isConstruct = false)
        {
            OperationResult<IEnumerable<AccClass>> result = new OperationResult<IEnumerable<AccClass>>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = AccClassServices.Current.Find(x => x.AccGroupId == groupId, isConstruct);

                    if (result.Data == null) { throw new Exception("Not rows found"); }
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpPost]
        [Route("~/LSIS/class/Save")]
        public OperationResult<IEnumerable<AccClass>> SaveClases(AccClass _class)
        {
            OperationResult<IEnumerable<AccClass>> result = new OperationResult<IEnumerable<AccClass>>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = AccClassServices.Current.Save(_class);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        #endregion

        #region Rubros Region

        [HttpGet]
        [Route("~/LSIS/rubro/GetAll")]
        public OperationResult<IEnumerable<AccRubro>> GetallRubros(bool isConstruct = false)
        {
            OperationResult<IEnumerable<AccRubro>> result = new OperationResult<IEnumerable<AccRubro>>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = AccRubroServices.Current.GetAll(isConstruct);

                    if (!result.Data.Any()) { throw new Exception("Not rows found"); }
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/rubro/FindById")]
        public OperationResult<AccRubro> FindRubrosById(Guid gid, bool isConstruct = false)
        {
            OperationResult<AccRubro> result = new OperationResult<AccRubro>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = AccRubroServices.Current.Find(gid, isConstruct);

                    if (result.Data == null) { throw new Exception("Not rows found"); }
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/rubro/FindRubrosByGroupId")]
        public OperationResult<IEnumerable<AccRubro>> FindRubrosByGroupId(Guid groupId, bool isConstruct = false)
        {
            OperationResult<IEnumerable<AccRubro>> result = new OperationResult<IEnumerable<AccRubro>>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = AccRubroServices.Current.Find(x => x.AccGroupGID == groupId, isConstruct);

                    if (result.Data == null) { throw new Exception("Not rows found"); }
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpPost]
        [Route("~/LSIS/rubro/Save")]
        public OperationResult<IEnumerable<AccRubro>> SaveRubros(AccRubro rubro)
        {
            OperationResult<IEnumerable<AccRubro>> result = new OperationResult<IEnumerable<AccRubro>>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = AccRubroServices.Current.Save(rubro);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        #endregion

        #region Catalog Region

        [HttpGet]
        [Route("~/LSIS/account/GetAll")]
        public OperationResult<IEnumerable<AccCatalog>> GetallAccount(bool isConstruct = false)
        {
            OperationResult<IEnumerable<AccCatalog>> result = new OperationResult<IEnumerable<AccCatalog>>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    //result = AccCatalogServices.Current.GetAll(isConstruct);
                    result = AccCatalogServices.Current.Find(x => x.ParentAccountId == Guid.Empty);

                    if (!result.Data.Any()) { throw new Exception("Not rows found"); }
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/account/GetParents")]
        public OperationResult<IEnumerable<ParentAccounts>> GetParents()
        {
            OperationResult<IEnumerable<ParentAccounts>> result = new OperationResult<IEnumerable<ParentAccounts>>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    //result = AccCatalogServices.Current.GetAll(isConstruct);
                    result = AccCatalogServices.Current.GetParents();

                    if (!result.Data.Any()) { throw new Exception("Not rows found"); }
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/account/FindById")]
        public OperationResult<AccCatalog> FindAccountById(Guid gid, bool isConstruct = false)
        {
            OperationResult<AccCatalog> result = new OperationResult<AccCatalog>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = AccCatalogServices.Current.Find(gid, isConstruct);

                    if (result.Data == null) { throw new Exception("Not rows found"); }
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpPost]
        [Route("~/LSIS/account/Save")]
        public OperationResult<IEnumerable<AccCatalog>> SaveAccount(AccCatalog accCatalog)
        {
            OperationResult<IEnumerable<AccCatalog>> result = new OperationResult<IEnumerable<AccCatalog>>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = AccCatalogServices.Current.Save(accCatalog);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        #endregion
    }
}
