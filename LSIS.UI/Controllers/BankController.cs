﻿using LSIS.ENTITY;
using LSIS.SERVICES;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LSIS.UI.Controllers
{
    public class BankController : ApiController
    {
        #region [Banks]

        [HttpGet]
        [Route("~/LSIS/AccBank/GetAll")]
        public OperationResult<IEnumerable<AccBank>> GetAllBanks(bool isContruct = false)
        {
            OperationResult<IEnumerable<AccBank>> result = new OperationResult<IEnumerable<AccBank>>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = AccBankServices.Current.GetAll(isContruct);
                }

            }
            catch (Exception ex)
            {
                result.SetFail(new Exception("Le session ha expirado"));
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/AccBank/FindById")]
        public OperationResult<AccBank> FinById(Guid gid, bool isContruct = false)
        {
            OperationResult<AccBank> result = new OperationResult<AccBank>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {

                    result = AccBankServices.Current.Find(gid, isContruct);

                }

            }
            catch (Exception ex)
            {
                result.SetFail(new Exception("Le session ha expirado"));
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/AccBank/FindByName")]
        public OperationResult<IEnumerable<AccBank>> FindByName(string name, bool isContruct = false)
        {
            OperationResult<IEnumerable<AccBank>> result = new OperationResult<IEnumerable<AccBank>>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {

                    result = AccBankServices.Current.Find(name, isContruct);

                }

            }
            catch (Exception ex)
            {
                result.SetFail(new Exception("Le session ha expirado"));
            }

            return result;
        }

        [HttpPost]
        [Route("~/LSIS/AccBank/Save")]
        public OperationResult<AccBank> SaveBank([FromBody]AccBank request)
        {
            OperationResult<AccBank> result = new OperationResult<AccBank>();

            //Get Headres information
            var headers = Request.Headers;

            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();
        
            try
            {
                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    var saveRes = AccBankServices.Current.Save(request);

                    if (!saveRes.Success) { throw new Exception(saveRes.Messages.Message); }

                    result.Success = true;

                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        #endregion

        #region [Bank Accounts]

        #endregion
    }
}
