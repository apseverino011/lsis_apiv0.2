﻿using LSIS.ENTITY;
using LSIS.ENTITY.DTOs;
using LSIS.SERVICES;
using LSIS.UTILITY;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LSIS.UI.Controllers
{
    public class PeriodStateController : ApiController
    {
        [HttpGet]
        [Route("~/LSIS/periodState/GetAll")]
        [SwaggerResponse(HttpStatusCode.OK, "", typeof(OperationResult<IEnumerable<PeriodState>>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "", typeof(Error))]
        public IHttpActionResult GetAll()
        {
            IHttpActionResult result;

            var resp = PeriodStateServices.Current.GetAll();

            if (resp.Success)
            {
                result = Ok(resp);
            }
            else
            {
                result = Content(HttpStatusCode.BadRequest, CustomErrors.GR01);
            }

            return result;
        }
    }
}
