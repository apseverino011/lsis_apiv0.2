﻿using LSIS.ENTITY;
using LSIS.SERVICES;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LSIS.UI.Controllers
{
    public class GeneralTypeController : ApiController
    {

        [HttpGet]
        [Route("~/LSIS/GeneralType/ValidateCode")]
        public OperationResult<bool> Find(string code)
        {
            OperationResult<bool> result = new OperationResult<bool>();

            //Get Headres information
            var headers = Request.Headers;

            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();

            try
            {
                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    var re = GeneralTypeServices.Current.ValidateCode(code, token);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }
            return result;
        }

        [HttpGet]
        [Route("~/LSIS/GeneralType/GetAll")]
        public OperationResult<IEnumerable<GeneralType>> GetAll(bool isConstruct = false)
        {
            OperationResult<IEnumerable<GeneralType>> result = new OperationResult<IEnumerable<GeneralType>>();

            //Get Headres information
            var headers = Request.Headers;

            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();

            try
            {
                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = GeneralTypeServices.Current.GetAll(isConstruct);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }
            return result;
        }

        [HttpGet]
        [Route("~/LSIS/GeneralType/GetAllParent")]
        public OperationResult<IEnumerable<GeneralType>> GetAllParent(bool isConstruct = false)
        {
            OperationResult<IEnumerable<GeneralType>> result = new OperationResult<IEnumerable<GeneralType>>();

            //Get Headres information
            var headers = Request.Headers;

            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();

            try
            {
                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result.Success = true;
                    result.Data = GeneralTypeServices.Current.Find(x => x.IsParent, isConstruct).Data.OrderBy(c => c.Description);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }
            return result;
        }

        [HttpGet]
        [Route("~/LSIS/GeneralType/FindChieldsByParentCode")]
        public OperationResult<IEnumerable<GeneralType>> FindByParentCode(string parentCode, bool isConstruct = false)
        {
            OperationResult<IEnumerable<GeneralType>> result = new OperationResult<IEnumerable<GeneralType>>();

            //Get Headres information
            var headers = Request.Headers;

            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();

            try
            {
                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = GeneralTypeServices.Current.FindChieldByParentCode(parentCode, isConstruct);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }
            return result;
        }

        [HttpGet]
        [Route("~/LSIS/GeneralType/FindByGid")]
        public OperationResult<GeneralType> FindByGid(Guid gtypeId, bool isConstruct = false)
        {
            OperationResult<GeneralType> result = new OperationResult<GeneralType>();

            //Get Headres information
            var headers = Request.Headers;

            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();

            try
            {
                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result.Success = true;

                    result.Data = GeneralTypeServices.Current.Find(x => x.GID == gtypeId, isConstruct).Data.FirstOrDefault();
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }
            return result;
        }

        [HttpGet]
        [Route("~/LSIS/GeneralType/FindChieldsByParentGid")]
        public OperationResult<IEnumerable<GeneralType>> FindByParentId(Guid parentId, bool isConstruct = false)
        {
            OperationResult<IEnumerable<GeneralType>> result = new OperationResult<IEnumerable<GeneralType>>();

            //Get Headres information
            var headers = Request.Headers;

            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();

            try
            {
                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = GeneralTypeServices.Current.FindChieldByParentGID(parentId, isConstruct);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }
            return result;
        }

        [HttpGet]
        [Route("~/LSIS/GeneralType/FindByCode")]
        public OperationResult<GeneralType> Find(string code, bool isConstruct = false)
        {
            OperationResult<GeneralType> result = new OperationResult<GeneralType>();

            //Get Headres information
            var headers = Request.Headers;

            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();

            try
            {
                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = GeneralTypeServices.Current.Find(code, isConstruct);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }
            return result;
        }

        [HttpPost]
        [Route("~/LSIS/GeneralType/Save")]
        public OperationResult<IEnumerable<GeneralType>> Save([FromBody]GeneralType generalType)
        {
            OperationResult<IEnumerable<GeneralType>> result = new OperationResult<IEnumerable<GeneralType>>();

            //Get Headres information
            var headers = Request.Headers;

            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();

            try
            {
                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = GeneralTypeServices.Current.Save(generalType);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpPost]
        [Route("~/LSIS/GeneralType/SaveList")]
        public OperationResult<IEnumerable<GeneralType>> SaveList([FromBody]List<GeneralType> generalTypes)
        {
            OperationResult<IEnumerable<GeneralType>> result = new OperationResult<IEnumerable<GeneralType>>();

            //Get Headres information
            var headers = Request.Headers;

            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();

            try
            {
                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = GeneralTypeServices.Current.Save(generalTypes);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }


    }
}
