﻿using LSIS.DATA;
using LSIS.ENTITY;
using LSIS.SERVICES;
using LSIS.UI.Models;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Cors;

namespace LSIS.UI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UsersController : ApiController
    {

        /// <summary>
        /// Authentication for app users
        /// </summary>
        /// <returns>User authentiacation</returns>
        [HttpPost]
        [Route("~/LSIS/Log/In")]
        public OperationResult<UserDto> Login([FromBody]_LoginDtos us)
        {
            OperationResult<UserDto> result = new OperationResult<UserDto>();

            UserDto userResult = new UserDto();
            List<RoleModulesOptionsDto> roleModules = new List<RoleModulesOptionsDto>();
            RoleModulesOptionsDto option = new RoleModulesOptionsDto();

            try
            {
                //Validate User
                var session = UserServices.Current._Login(us.UserName, us.Password, company: Guid.Empty);

                if (session.Success)
                {
                    var sess = session.Data.FirstOrDefault();

                    userResult.Token = GeneralInfo.Current.GenerateToken(sess.UserId.ToString(), sess.GID.ToString());

                    //Get User Information
                    var userInf = UserServices.Current.Find(x => x.GID == sess.UserId).Data.FirstOrDefault();

                    userResult.UserName = userInf.FullName;

                    //Set Session Time
                    userResult.TimeSession = int.Parse(WebConfigurationManager.AppSettings["ST"].ToString());

                    //Get Modules and contruct menu
                    var modules = RoleModulesOptionServices.Current.FindByUserId(userInf.UserRoleGID, isConstruct: true);
                    //var modules = RoleModuleServices.Current.Find(userInf.UserRoleGID, isConstruct: true);

                    userResult.Modules = this.ConstructMenu(modules.Data).OrderByDescending(c => c.Sort).ToList();
                    userResult.Language = userInf.Language;

                    result.Data = userResult;
                }
                else
                {
                    throw new Exception("El Usuario o la contraseña son incorrectos");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpPost]
        [Route("~/LSIS/Log/Out")]
        public void LogOut()
        {
            var headers = Request.Headers;

            //Get parameters from header.
            string token = headers.GetValues("Token").FirstOrDefault();

            try
            {
                string[] sessionId = GeneralInfo.Current.GetTokenInformation(token);

                UserServices.Current._LogOut(Guid.Parse(sessionId[1]));

            }
            catch (Exception)
            {
                throw;
            }

            //return result;
        }

        [HttpGet]
        [Route("~/LSIS/User/ValidateUserName")]
        public OperationResult<bool> ValidateUserName(string userName)
        {
            OperationResult<bool> result = new OperationResult<bool>();

            //Get Headres information
            var headers = Request.Headers;
            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();

            try
            {
                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    if (UserServices.Current.Find(x => x.UsrId == userName && x.CompanyId == InternalInfo.CompanyId).Data.Any())
                    {
                        result.Success = true;
                        //If User exists return true
                        result.Data = true;
                    }
                    else
                    {
                        result.Success = true;
                        //If User not exists return true
                        result.Data = false;
                    }
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/User/GetAll")]
        public OperationResult<IEnumerable<User>> GetAll(bool isConstruct = false)
        {
            OperationResult<IEnumerable<User>> result = new OperationResult<IEnumerable<User>>();

            //Get Headres information
            var headers = Request.Headers;
            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();

            try
            {
                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = UserServices.Current.GetAll(token, isConstruct);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/User/GetUserById")]
        public OperationResult<User> GetUserById(Guid gid, bool isConstruct = false)
        {
            OperationResult<User> result = new OperationResult<User>();

            //Get Headres information
            var headers = Request.Headers;
            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();

            try
            {
                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result.Data = UserServices.Current.Find(gid, isConstruct).Data.FirstOrDefault();
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpPost]
        [Route("~/LSIS/User/Save")]
        public OperationResult<User> Save([FromBody]User user)
        {
            OperationResult<User> result = new OperationResult<User>();

            //Get Headres information
            var headers = Request.Headers;
            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();

            try
            {
                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result.Data = UserServices.Current.Save(user).Data.FirstOrDefault();
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        #region [Internal Methods]

        private List<RoleModulesOptionsDto> ConstructMenu(IEnumerable<RoleModulesOption> modules)
        {
            List<RoleModulesOptionsDto> result = new List<RoleModulesOptionsDto>();

            List<RoleModulesOptionsDto> opts = new List<RoleModulesOptionsDto>();

            foreach (RoleModulesOption option in modules)
            {
                if (!option.IsSub)
                {
                    opts.Add(new RoleModulesOptionsDto()
                    {
                        GID = option.Module.GID,
                        Role = string.Empty,
                        Name = option.Module.Name,
                        Link = option.Module.Link,
                        _select = option._Select,
                        _update = option._Update,
                        _delete = option._Delete,
                        _insert = option._Insert,
                        Sort = option.Module.Sort,

                        Owner = option.Module.Owner,
                        IsMain = option.Module.IsMain,
                        IsSubM = option.Module.IsSubM
                    });
                }

                opts.Add(new RoleModulesOptionsDto()
                {
                    GID = option.Option.GID,
                    Role = string.Empty,
                    Name = option.Option.Name,
                    Link = option.Option.Link,
                    _select = option._Select,
                    _update = option._Update,
                    _delete = option._Delete,
                    _insert = option._Insert,
                    Sort = option.Option.Sort,

                    Owner = option.Option.Owner,
                    IsMain = option.Option.IsMain,
                    IsSubM = option.Option.IsSubM
                });

            }

            var moduls = opts.Select(x => new { x.GID, x.Name, x.IsMain, x.IsSubM }).Where(c => (c.IsMain)).Distinct();


            foreach (var item in moduls)
            {

                var opt = new RoleModulesOptionsDto()
                {
                    GID = item.GID,
                    Name = item.Name,
                    Options = opts.Where(c => c.Owner == item.GID).OrderBy(cc => cc.Sort).ToList()
                };

                opt.Options.ForEach(x => { if (x.IsSubM) { x.Options = opts.Where(c => c.Owner == x.GID).OrderBy(cc => cc.Sort).ToList(); } });

                result.Add(opt);
            }
            return result;

        }

        //private List<RoleModulesOptionsDto> ConstructMenu(IEnumerable<RoleModulesOption> modules)
        //{
        //    List<RoleModulesOptionsDto> result = new List<RoleModulesOptionsDto>();

        //    var _modules = modules.Where(c => !c.IsSub).Select(c=>c.ModuleId).Distinct().ToList();

        //    var _options = modules.Select(c => c.OptionId).ToList();

        //    return result;
        //}

        #endregion

    }
}
