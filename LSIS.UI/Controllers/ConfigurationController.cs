﻿using LSIS.ENTITY;
using LSIS.SERVICES;
using LSIS.UI.Models;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace LSIS.UI.Controllers
{
    //    [EnableCors(origins:"*",headers:"*",methods:"*")]
    public class ConfigurationController : ApiController
    {
        #region MoveFile
        [HttpPost]
        [Route("~/LSIS/File/UploadFile")]
        public string AddAudio(HttpPostedFile filePost)
        {
            string lfilename = filePost.FileName;
            string lfilepath = "~/Temp" + lfilename;
            filePost.SaveAs(lfilepath);
            return lfilepath;
        }
        //public HttpResponseMessage Post(string filename)
        //{
        //    var task = this.Request.Content.ReadAsStreamAsync();
        //    task.Wait();
        //    Stream requestStream = task.Result;

        //    try
        //    {
        //        Stream fileStream = File.Create(HttpContext.Current.Server.MapPath("~/Temp" + filename));
        //        requestStream.CopyTo(fileStream);
        //        fileStream.Close();
        //        requestStream.Close();
        //    }
        //    catch (IOException)
        //    {
        //        //throw new HttpResponseException("A generic error occured. Please try again later.", HttpStatusCode.InternalServerError);
        //    }

        //    HttpResponseMessage response = new HttpResponseMessage();
        //    response.StatusCode = HttpStatusCode.Created;
        //    return response;
        //}
        #endregion

        #region [Modules]

        [HttpGet]
        [Route("~/LSIS/Modules/GetAllModuleCompany")]
        public OperationResult<IEnumerable<GeneralModule>> GetAllModuleCompany(bool isContruct = false)
        {
            OperationResult<IEnumerable<GeneralModule>> result = new OperationResult<IEnumerable<GeneralModule>>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {

                    result = GeneralModuleServices.Current.GetAllModulesCompanies(token, isContruct);

                }
                else
                {
                    throw new Exception("Le session ha expirado");
                }

            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/Modules/GetByGID")]
        public OperationResult<GeneralModule> GetByGID(Guid gid, bool isContruct = false)
        {
            OperationResult<GeneralModule> result = new OperationResult<GeneralModule>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result.Data = GeneralModuleServices.Current.Find(x => x.GID == gid, isContruct).Data.FirstOrDefault();

                }
                else
                {
                    throw new Exception("Le session ha expirado");
                }

            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/Modules/GetAllMainModules")]
        public OperationResult<IEnumerable<GeneralModule>> GetAllMainModules(bool isContruct = false)
        {
            OperationResult<IEnumerable<GeneralModule>> result = new OperationResult<IEnumerable<GeneralModule>>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {

                    result = GeneralModuleServices.Current.GetAllMainModules(isContruct);

                }
                else
                {
                    throw new Exception("Le session ha expirado");
                }

            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/Modules/GetAllSubModules")]
        public OperationResult<IEnumerable<GeneralModule>> GetAllSubModules(bool isContruct = false)
        {
            OperationResult<IEnumerable<GeneralModule>> result = new OperationResult<IEnumerable<GeneralModule>>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {

                    result = GeneralModuleServices.Current.GetAllSubModules(isContruct);

                }
                else
                {
                    throw new Exception("Le session ha expirado");
                }

            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/Modules/GetAllOptions")]
        public OperationResult<IEnumerable<GeneralModule>> GetAllOptions(bool isContruct = false)
        {
            OperationResult<IEnumerable<GeneralModule>> result = new OperationResult<IEnumerable<GeneralModule>>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {

                    result = GeneralModuleServices.Current.GetAllOptions(isContruct);

                }
                else
                {
                    throw new Exception("Le session ha expirado");
                }

            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/Modules/ValidateModuleName")]
        public OperationResult<GeneralModule> ValidateModuleName(string name)
        {
            OperationResult<GeneralModule> result = new OperationResult<GeneralModule>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {

                    result = GeneralModuleServices.Current.ValidateModuleName(name);

                }
                else
                {
                    throw new Exception("Le session ha expirado");
                }

            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/Modules/ValidateOptionName")]
        public OperationResult<GeneralModule> ValidateOptionName(string name, Guid owner)
        {
            OperationResult<GeneralModule> result = new OperationResult<GeneralModule>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {

                    result = GeneralModuleServices.Current.ValidateOptionName(name, owner);

                }
                else
                {
                    throw new Exception("Le session ha expirado");
                }

            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpPost]
        [Route("~/LSIS/Modules/CreateModule")]
        public OperationResult<GeneralModule> CreateModule(GeneralModule module)
        {
            return this.CreateModule(new List<GeneralModule>() { module });
        }

        [HttpPost]
        [Route("~/LSIS/Modules/CreateModuleList")]
        public OperationResult<GeneralModule> CreateModule(List<GeneralModule> modules)
        {
            OperationResult<GeneralModule> result = new OperationResult<GeneralModule>();

            //Get Headres information
            var headers = Request.Headers;

            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();

            try
            {
                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    if (!GeneralModuleServices.Current.Save(modules).Success) { throw new Exception("The Modules cannot be saved."); }

                    result.Success = true;

                }
                else
                {
                    throw new Exception("La session ha expirado");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }
        #endregion [Modules]

        #region [Roles]

        [HttpGet]
        [Route("~/LSIS/UserRoles/GetRoleToEdit")]
        public OperationResult<UsersRole> GetRoleToEdit(Guid roleId)
        {
            OperationResult<UsersRole> result = new OperationResult<UsersRole>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    UsersRole role = UserRoleServices.Current.Find(x => x.GID == roleId, isConstruct: true).Data.FirstOrDefault();


                    var modules = role.Options.Select(c => new { c.UserRoleId, c.ModuleId, c.IsMain, c.IsSub }).Distinct();

                    var options = role.Options.Select(c => new { c.UserRoleId, c.OptionId}).Distinct();


                    var companyModules = GeneralModuleServices.Current.GetAllModulesCompanies(token, isConstruct: true).Data.ToList();

                    companyModules.ForEach(x =>
                    {
                        if (modules.Any(c => c.ModuleId == x.GID && c.IsMain))
                        {
                            x.Select = true;
                            x.Options.ForEach(xc => { if (options.Any(cx => cx.OptionId == xc.GID)) { xc.Select = true; } });
                        }

                        x.Options.ForEach(cc =>
                        {
                            if (modules.Any(c => c.ModuleId == cc.GID && c.IsSub))
                            {
                                cc.Select = true;
                                cc.Options.ForEach(xc => { if (options.Any(cx => cx.OptionId == xc.GID)){ xc.Select = true; } });
                            }
                        });

                    });


                    role.EditOptions = companyModules;
                    result.Data = role;


                }

            }
            catch (Exception ex)
            {
                result.SetFail(new Exception("Le session ha expirado"));
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/UserRoles/ValidateRoleName")]
        public OperationResult<UsersRole> ValidateRoleName(string name)
        {
            OperationResult<UsersRole> result = new OperationResult<UsersRole>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                   

                }

            }
            catch (Exception ex)
            {
                result.SetFail(new Exception("Le session ha expirado"));
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/UserRoles/FindBy")]
        public OperationResult<IEnumerable<UsersRole>> GetUserRolesByCompany(bool isConstruct = false)
        {
            OperationResult<IEnumerable<UsersRole>> result = new OperationResult<IEnumerable<UsersRole>>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = UserRoleServices.Current.FindByCompany(res.Data.GID);

                }

            }
            catch (Exception ex)
            {
                result.SetFail(new Exception("Le session ha expirado"));
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/roles/GetAll")]
        public OperationResult<IEnumerable<RoleModulesOption>> GetAllRoles(bool isConstruct = false)
        {
            return RoleModulesOptionServices.Current.GetAll(isConstruct);
        }

        [HttpGet]
        [Route("~/LSIS/roles/FindRollByCompany")]
        public OperationResult<IEnumerable<RoleModulesOptionsDto>> GetRolesByCompany(bool isConstruct = false)
        {
            var headers = Request.Headers;

            string token = headers.GetValues("Token").FirstOrDefault();

            return new OperationResult<IEnumerable<RoleModulesOptionsDto>>();
        }

        [HttpGet]
        [Route("~/LSIS/roles/GetRolByGid")]
        public OperationResult<IEnumerable<string>> GetRolByGid(Guid rol, bool isConstruct = false)
        {
            return new OperationResult<IEnumerable<string>>();
        }

        [HttpGet]
        [Route("~/LSIS/roles/GetRolByName")]
        public OperationResult<IEnumerable<string>> GetRolByName(bool isConstruct = false)
        {
            return new OperationResult<IEnumerable<string>>();
        }

        [HttpPost]
        [Route("~/LSIS/roles/SaveRol")]
        public OperationResult<IEnumerable<UsersRole>> SaveRol([FromBody]UsersRole role)
        {
            OperationResult<IEnumerable<UsersRole>> result = new OperationResult<IEnumerable<UsersRole>>();

            var headers = Request.Headers;
            try
            {
                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    role.CompanyId = res.Data.GID;

                    result = RoleModulesOptionServices.Current.Save(role);
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }
            return result;
        }

        #endregion [Roles]

    }
}
