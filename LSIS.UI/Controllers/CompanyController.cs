﻿using LSIS.ENTITY;
using LSIS.SERVICES;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LSIS.UI.Controllers
{
    public class CompanyController : ApiController
    {
        [HttpGet]
        [Route("~/LSIS/Company/GetAll")]
        public OperationResult<IEnumerable<Company>> GetAll()
        {
            OperationResult<IEnumerable<Company>> result = new OperationResult<IEnumerable<Company>>();

            //Get Headres information
            var headers = Request.Headers;

            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();
            try
            {

                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = CompanyServices.Current.GetAll();
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/Company/GetEmployeeByCode")]
        public OperationResult<Party> GetEmployeeByCode(string code)
        {
            OperationResult<Party> result = new OperationResult<Party>();
            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = CompanyServices.Current.GetEmployeeByCode(code);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/Company/FindCompanyById")]
        public OperationResult<Company> FindCompanyById(Guid companyId, bool isConstruct = false)
        {
            OperationResult<Company> result = new OperationResult<Company>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result.Data = CompanyServices.Current.Find(x=> x.GID == companyId, isConstruct).Data.FirstOrDefault();

                    if(result.Data == null) { throw new Exception("Company not found"); }
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/Company/GetCompanyEmployees")]
        public OperationResult<IEnumerable<CompanyEmploye>> GetCompanyEmployees(Guid companyId)
        {
            OperationResult<IEnumerable<CompanyEmploye>> result = new OperationResult<IEnumerable<CompanyEmploye>>();
            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    //result = CompanyServices.Current.GetEmployeeByCode(code);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/Company/GetCompanyClients")]
        public OperationResult<IEnumerable<Party>> GetCompanyClients(Guid companyId)
        {
            OperationResult<IEnumerable<Party>> result = new OperationResult<IEnumerable<Party>>();
            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    //result = CompanyServices.Current.GetEmployeeByCode(code);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/Company/FindByName")]
        public OperationResult<IEnumerable<Company>> FindByName(string companyName, bool isConstruct = false)
        {
            OperationResult<IEnumerable<Company>> result = new OperationResult<IEnumerable<Company>>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    //result = CompanyServices.Current.GetEmployeeByCode(code);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpPost]
        [Route("~/LSIS/Company/Save")]
        public OperationResult<Company> save([FromBody]Company company)
        {
            OperationResult<Company> result = new OperationResult<Company>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = CompanyServices.Current.Save(company);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }
    }
}
