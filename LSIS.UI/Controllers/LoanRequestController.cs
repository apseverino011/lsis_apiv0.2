﻿using LSIS.ENTITY;
using LSIS.SERVICES;
using LSIS.UI.Models;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LSIS.UI.Controllers
{
    public class LoanRequestController : ApiController
    {

        [HttpGet]
        [Route("~/LSIS/LoanRequest/GetAll")]
        public OperationResult<IEnumerable<LoanRequest>> GetAll(bool isContruct = false)
        {
            OperationResult<IEnumerable<LoanRequest>> result = new OperationResult<IEnumerable<LoanRequest>>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {

                    result = LoanRequestServices.Current.GetAll(res.Data.GID);

                }

            }
            catch (Exception ex)
            {
                result.SetFail(new Exception("Le session ha expirado"));
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/LoanRequest/FindByNumber")]
        public OperationResult<IEnumerable<LoanRequest>> FinByNumber(string number, bool isContruct = false)
        {
            OperationResult<IEnumerable<LoanRequest>> result = new OperationResult<IEnumerable<LoanRequest>>();

            try
            {
                var headers = Request.Headers;

                string token = headers.GetValues("Token").FirstOrDefault();

                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {

                    result = LoanRequestServices.Current.Find(number, isContruct);

                }

            }
            catch (Exception ex)
            {
                result.SetFail(new Exception("Le session ha expirado"));
            }

            return result;
        }

        [HttpPost]
        [Route("~/LSIS/LoanRequest/CreateRequest")]
        public OperationResult<LoanRequest> Create([FromBody]LoanRequest request)
        {
            OperationResult<LoanRequest> result = new OperationResult<LoanRequest>();

            //Get Headres information
            var headers = Request.Headers;

            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();

        
            try
            {
                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    if (!LoanRequestServices.Current.Save(request).Success) { throw new Exception("The request cannot be saved."); }

                    result.Success = true;

                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

    }
}
