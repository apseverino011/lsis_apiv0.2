﻿using LSIS.ENTITY;
using LSIS.ENTITY.DTOs;
using LSIS.ENTITY.Views;
using LSIS.SERVICES;
using LSIS.UTILITY;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LSIS.UI.Controllers
{
    public class PeriodsController : ApiController
    {
        [HttpGet]
        [Route("~/LSIS/period/GetPeriods")]
        [SwaggerResponse(HttpStatusCode.OK, "", typeof(IEnumerable<Periods>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "", typeof(Error))]
        public IHttpActionResult GetPeriods()
        {
            IHttpActionResult result;

            var resp = PeriodsServices.Current.GetPerios();

            if (resp.Success)
            {
                result = Ok(resp.Data);
            }
            else
            {
                result = Content(HttpStatusCode.BadRequest, CustomErrors.PE01);
            }

            return result;
        }
        
        [HttpPut]
        [Route("~/LSIS/accountPeriod/LockPeriod/{periodId}")]
        [SwaggerResponse(HttpStatusCode.OK, "", typeof(OperationResult<AccPeriods>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "", typeof(Error))]
        public IHttpActionResult LockPeriod(Guid periodId)
        {
            IHttpActionResult result;

            var resp = PeriodsServices.Current.LockPeriod(periodId);

            if (resp.Success)
            {
                result = Ok(resp);
            }
            else
            {
                result = Content(HttpStatusCode.BadRequest, CustomErrors.PE01);
            }

            return result;
        }

        [HttpPut]
        [Route("~/LSIS/accountPeriod/ClosePeriod/{periodId}")]
        [SwaggerResponse(HttpStatusCode.OK, "", typeof(OperationResult<AccPeriods>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "", typeof(Error))]
        public IHttpActionResult ClosePeriod(Guid periodId)
        {
            IHttpActionResult result;

            var resp = PeriodsServices.Current.ClosePeriod(periodId);

            if (resp.Success)
            {
                result = Ok(resp);
            }
            else
            {
                result = Content(HttpStatusCode.BadRequest, CustomErrors.PE01);
            }

            return result;
        }

        [HttpPut]
        [Route("~/LSIS/accountPeriod/LockPeriodDetail/{periodDetailId}")]
        [SwaggerResponse(HttpStatusCode.OK, "", typeof(OperationResult<AccPeriods>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "", typeof(Error))]
        public IHttpActionResult LockPeriodDetail(Guid periodDetailId)
        {
            IHttpActionResult result;

            var resp = PeriodsServices.Current.LockPeriodDetail(periodDetailId);

            if (resp.Success)
            {
                result = Ok(resp);
            }
            else
            {
                result = Content(HttpStatusCode.BadRequest, CustomErrors.PE01);
            }

            return result;
        }

        [HttpPut]
        [Route("~/LSIS/accountPeriod/ClosePeriodDetil/{periodDetailId}")]
        [SwaggerResponse(HttpStatusCode.OK, "", typeof(OperationResult<AccPeriods>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "", typeof(Error))]
        public IHttpActionResult ClosePeriodDetail(Guid periodDetailId)
        {
            IHttpActionResult result;

            var resp = PeriodsServices.Current.ClosePeriodDetail(periodDetailId);

            if (resp.Success)
            {
                result = Ok(resp);
            }
            else
            {
                result = Content(HttpStatusCode.BadRequest, CustomErrors.PE01);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/accountPeriod/Find")]
        [SwaggerResponse(HttpStatusCode.OK, "", typeof(OperationResult<IEnumerable<AccPeriods>>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "", typeof(Error))]
        public IHttpActionResult Find(Guid gid, bool isConstruct = false)
        {
            IHttpActionResult result;

            var resp = PeriodsServices.Current.Find(gid, isConstruct);

            if (resp.Success)
            {
                result = Ok(resp);
            }
            else
            {
                result = Content(HttpStatusCode.BadRequest, CustomErrors.PE01);
            }

            return result;
        }

        [HttpPost]
        [Route("~/LSIS/accountPeriod/Save")]
        [SwaggerResponse(HttpStatusCode.OK, "", typeof(OperationResult<AccPeriods>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, "", typeof(Error))]
        public IHttpActionResult Save([FromBody]AccPeriods periods)
        {
            IHttpActionResult result;

            //Get Headres information
            var headers = Request.Headers;

            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();

            //Validate Token
            var res = GeneralServices.Current.ValidateToken(token);

            if (res.Success)
            {
                var resp = PeriodsServices.Current.Save(periods);

                if (resp.Success)
                {
                    result = Ok(resp);
                }
                else
                {
                    result = Content(HttpStatusCode.BadRequest, CustomErrors.PE01);
                }
            }
            else
            {
                result = Content(HttpStatusCode.BadRequest, CustomErrors.PE01);
            }
            return result;
        }
    }
}
