﻿using LSIS.ENTITY;
using LSIS.SERVICES;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LSIS.UI.Controllers
{
    public class PartyController : ApiController
    {

        [HttpGet]
        [Route("~/LSIS/Party/GetAll")]
        public OperationResult<IEnumerable<Party>> GetAll(bool isConstruct = false)
        {
            OperationResult<IEnumerable<Party>> result = new OperationResult<IEnumerable<Party>>();

            //Get Headres information
            var headers = Request.Headers;

            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();

            try
            {
                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = PartyServices.Current.GetAll(isConstruct);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/Party/FindByName")]
        public OperationResult<IEnumerable<Party>> Find(string name, bool isConstruct = false)
        {
            OperationResult<IEnumerable<Party>> result = new OperationResult<IEnumerable<Party>>();

            //Get Headres information
            var headers = Request.Headers;

            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();

            try
            {
                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = PartyServices.Current.Find(name, isConstruct);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/Party/FindByIdent")]
        public OperationResult<IEnumerable<Party>> FindByIdent(string identNumber, bool isConstruct = false)
        {
            OperationResult<IEnumerable<Party>> result = new OperationResult<IEnumerable<Party>>();

            //Get Headres information
            var headers = Request.Headers;

            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();

            try
            {
                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    //result = PartyServices.Current.FindByIdent(identNumber, isConstruct);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpGet]
        [Route("~/LSIS/Party/FindByGid")]
        public OperationResult<IEnumerable<Party>> Find(Guid gid, bool isConstruct = false)
        {
            OperationResult<IEnumerable<Party>> result = new OperationResult<IEnumerable<Party>>();

            //Get Headres information
            var headers = Request.Headers;

            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();

            try
            {
                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    result = PartyServices.Current.Find(gid, isConstruct);
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpPost]
        [Route("~/LSIS/Party/Save")]
        public OperationResult<IEnumerable<Party>> Save([FromBody]Party party)
        {
            OperationResult<IEnumerable<Party>> result = new OperationResult<IEnumerable<Party>>();

            //Get Headres information
            var headers = Request.Headers;

            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();

            try
            {
                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    if (!PartyServices.Current.Save(party).Success) { throw new Exception("The party cannot be saved."); }

                    result.Success = true;
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }

        [HttpPost]
        [Route("~/LSIS/Party/Save")]
        public OperationResult<IEnumerable<Party>> Save([FromBody]List<Party> parties)
        {
            OperationResult<IEnumerable<Party>> result = new OperationResult<IEnumerable<Party>>();

            //Get Headres information
            var headers = Request.Headers;

            //Get Token value
            string token = headers.GetValues("Token").FirstOrDefault();

            try
            {
                //Validate Token
                var res = GeneralServices.Current.ValidateToken(token);

                if (res.Success)
                {
                    if (!PartyServices.Current.Save(parties).Success) { throw new Exception("The parties cannot be saved."); }

                    result.Success = true;
                }
                else
                {
                    throw new Exception("The token can't be validated");
                }
            }
            catch (Exception ex)
            {
                result.SetFail(ex);
            }

            return result;
        }
    }
}
