using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("Parties")]
    public partial class Party : EntityBase
    {
        [Required]
        [StringLength(100)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        public string LastName { get; set; }

        [Required]
        [StringLength(200)]
        public string FullName { get; set; }

        public Guid Gender { get; set; }

        public Guid? MaritalStatus { get; set; }

        public DateTime? DoB { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        [NotMapped]
        public List<PartyIdentification> Identifications { get; set; }

        [NotMapped]
        public List<ContactNumber> ContactNumbers { get; set; }

        [NotMapped]
        public List<Location> PartyLocations { get; set; }

        [NotMapped]
        public List<Party> PersonsContact { get; set; }

    }
}
