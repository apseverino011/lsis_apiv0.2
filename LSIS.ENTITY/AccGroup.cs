using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("AccGroups")]
    public partial class AccGroup: EntityBase
    {
        public Guid AccOriginId { get; set; }

        [Required]
        [StringLength(5)]
        public string AccGroupCode { get; set; }

        [StringLength(100)]
        public string AccGroupName { get; set; }

        public Guid AccTypeId { get; set; }

        public int NextAccNumber { get; set; }

        public int NextSubAccNumber { get; set; }
    }
}
