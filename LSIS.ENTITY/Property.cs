using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("Properties")]
    public partial class Property : EntityBase
    {
        public Guid Type { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Leves { get; set; }

        public decimal Meters { get; set; }

        public Guid Address { get; set; }

        public bool? HasPool { get; set; }
    }
}
