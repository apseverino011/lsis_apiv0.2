﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.ENTITY
{
    public partial class EntityBase
    {

        [Required]
        public Guid GID { get; set; }
        [Key]
        [Required]
        public int Indx { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public Guid Status { get; set; }
        [Required]
        public DateTime LogDate { get; set; }
        public Nullable<Guid> UserId { get; set; }
        public Nullable<Guid> CompanyId { get; set; }

    }
}
