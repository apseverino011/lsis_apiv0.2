using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("AccBankAccounts")]
    public partial class AccBankAccount: EntityBase
    {

        public Guid BankId { get; set; }

        public Guid Type { get; set; }

        [StringLength(15)]
        public string BankAccountNumber { get; set; }

        public Guid AccountingId { get; set; }

    }
}
