using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("PartyIdentifications")]
    public partial class PartyIdentification: EntityBase
    {
        public Guid Party { get; set; }

        public Guid IdentificationType { get; set; }

        [Required]
        [StringLength(30)]
        public string Number { get; set; }

        public Guid? SubSectorGID { get; set; }

        public DateTime ExpirationDate { get; set; }

        [NotMapped]
        public string IdentificationTypeName { get; set; }
    }
}
