using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("RoleModulesOptions")]
    public partial class RoleModulesOption: EntityBase
    {
        public Guid UserRoleId { get; set; }

        public bool IsMain { get; set; }

        public bool IsSub { get; set; }

        public Guid ModuleId { get; set; }

        public Guid OptionId { get; set; }

        public bool _Select { get; set; }

        public bool _Update { get; set; }

        public bool _Delete { get; set; }

        public bool _Insert { get; set; }

        [NotMapped]
        public GeneralModule Module { get; set; }

        [NotMapped]
        public GeneralModule Option { get; set; }
    }
}
