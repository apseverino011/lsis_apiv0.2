using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("SessionHistory")]
    public partial class SessionHistory : EntityBase
    {
        [Required]
        [StringLength(16)]
        public string Ip { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public decimal? SessionTime { get; set; }

        public DateTime? LastUpdate { get; set; }

        [StringLength(25)]
        public string MacAddress { get; set; }
    }
}
