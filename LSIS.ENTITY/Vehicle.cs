using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("Vehicles")]
    public partial class Vehicle: EntityBase
    {
        public Guid BrandGID { get; set; }

        public Guid ModelGID { get; set; }

        [Required]
        [StringLength(15)]
        public string Color { get; set; }

        public int Year { get; set; }

        [Required]
        [StringLength(8)]
        public string LicensePlate { get; set; }

        [Required]
        [StringLength(45)]
        public string VIN { get; set; }

        [StringLength(10)]
        public string Enrollment { get; set; }
    }
}
