using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("AccBanks")]
    public partial class AccBank : EntityBase
    {
        [StringLength(15)]
        public string RNC { get; set; }

        [StringLength(25)]
        public string Name { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        [NotMapped]
        public List<AccBankAccount> Accounts { get; set; }

    }
}
