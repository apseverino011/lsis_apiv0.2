using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("AccCatalogs")]
    public partial class AccCatalog: EntityBase
    {
        [StringLength(15)]
        public string AccFullNumber { get; set; }

        [Required]
        [StringLength(4)]
        public string AccNumber { get; set; }

        [Required]
        [StringLength(100)]
        public string AccName { get; set; }

        public Guid AccGroupId { get; set; }

        public Guid AccClassId { get; set; }

        public Guid AccRubroId { get; set; }

        public Guid AccOriginId { get; set; }

        public bool? IsGeneralLedgerAcc { get; set; }

        public bool? IsBalanceSheetAcc { get; set; }

        public Guid AccBalanceSheetFunctionId { get; set; }

        public bool? IsProfitAndLossAcc { get; set; }

        public bool? IsCashFlowAcc { get; set; }

        public Guid? ParentAccountId { get; set; }

        //[NotMapped]
        //public AccGroup AccGroup { get; set; }

        //[NotMapped]
        //public AccClass AccClass { get; set; }

        //[NotMapped]
        //public AccRubro AccRubro { get; set; }

        [NotMapped]
        public AccCatalog ParentAccount { get; set; }

        [NotMapped]
        public List<AccCatalog> SubAccounts { get; set; }
    }
}
