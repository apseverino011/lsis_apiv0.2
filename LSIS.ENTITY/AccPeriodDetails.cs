﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.ENTITY
{
    [Table("AccPeriodDetails")]
    public partial class AccPeriodDetails: EntityBase
    {
        public Guid PeriodId { get; set; }

        [Required]
        [StringLength(15)]
        public string Name { get; set; }

        public Guid PeriodState { get; set; }
    }
}
