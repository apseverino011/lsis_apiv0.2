﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.ENTITY
{
    [Table(name: "CompanyEmployees")]
    public partial class CompanyEmploye : EntityBase
    {
        [StringLength(50)]
        public string CodeEmployee { get; set; }

        public Guid PartyGID { get; set; }

        public Guid Department { get; set; }

        public Guid? Position { get; set; }

        public decimal Salary { get; set; }


        //Not mapped properties
        [NotMapped]
        public Company Company { get; set; }

        [NotMapped]
        public Party Party { get; set; }

    }
}
