using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("UsersRoles")]
    public partial class UsersRole: EntityBase
    {
        [Required]
        [StringLength(35)]
        public string Name { get; set; }

        [Required]
        [StringLength(150)]
        public string Description { get; set; }

        [NotMapped]
        public List<RoleModulesOption> Options { get; set; }
        
        [NotMapped]
        public List<GeneralModule> EditOptions { get; set; }
    }
}
