﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LSIS.ENTITY
{
    [Table(name: "HistoricalChanges")]
    public partial class HistoricalChange : EntityBase
    {

        [Required]
        [StringLength(150)]
        public string TableName { get; set; }

        public Guid RowId { get; set; }

        [Required]
        [StringLength(100)]
        public string PropertyChanged { get; set; }

        [Required]
        public string OldValue { get; set; }

        [Required]
        public string NewValue { get; set; }

    }
}
