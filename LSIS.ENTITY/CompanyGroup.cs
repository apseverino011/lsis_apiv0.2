using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("CompanyGroups")]
    public partial class CompanyGroup: EntityBase
    {
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(350)]
        public string Description { get; set; }

        [NotMapped]
        public List<Company> Companies { get; set; }
    }
}
