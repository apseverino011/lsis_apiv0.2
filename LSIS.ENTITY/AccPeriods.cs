﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.ENTITY
{
    [Table("AccPeriods")]
    public partial class AccPeriods: EntityBase
    {
        [Required]
        [StringLength(25)]
        public string Name { get; set; }

        [StringLength(350)]
        public string Description { get; set; }

        [Required]
        [StringLength(10)]
        public string StartDate { get; set; }

        [Required]
        [StringLength(10)]
        public string EndDate { get; set; }

        public Guid PeriodState { get; set; }


        [NotMapped]
        public List<AccPeriodDetails> PeriodDetails { get; set; }
    }
}
