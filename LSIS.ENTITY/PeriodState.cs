using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("PeriodState")]
    public partial class PeriodState: EntityBase
    {
        [Required]
        [StringLength(10)]
        public string Code { get; set; }

        [Required]
        [StringLength(25)]
        public string Name { get; set; }

        [StringLength(350)]
        public string Description { get; set; }
    }
}
