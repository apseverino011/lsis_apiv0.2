using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("Users")]
    public partial class User: EntityBase
    {
        public Guid PartyGID { get; set; }

        [Required]
        [StringLength(200)]
        public string FullName { get; set; }

        [Required]
        [StringLength(20)]
        public string UsrId { get; set; }

        [Required]
        public string PssId { get; set; }

        [Required]
        [StringLength(5)]
        public string Language { get; set; }

        public Guid UserRoleGID { get; set; }

        public DateTime? ExpirationDate { get; set; }

        [NotMapped]
        public Party Party { get; set; }

        [NotMapped]
        public UsersRole Role { get; set; }
    }
}
