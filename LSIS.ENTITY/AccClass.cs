using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{

    [Table("AccClasses")]
    public partial class AccClass : EntityBase
    {
        public Guid AccGroupId { get; set; }

        [Required]
        [StringLength(5)]
        public string AccClassCode { get; set; }

        [Required]
        [StringLength(100)]
        public string AccClassName { get; set; }

        [NotMapped]
        public AccGroup AccGroup { get; set; }

    }
}
