using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("GeneralModules")]
    public partial class GeneralModule : EntityBase
    {
        public int Sort { get; set; }

        [Required]
        [StringLength(35)]
        public string Name { get; set; }

        [StringLength(75)]
        public string Link { get; set; }

        public bool IsMain { get; set; }

        public bool IsSubM { get; set; }

        public Guid? Owner { get; set; }

        //Virtual
        [NotMapped]
        public bool Select { get; set; }

        [NotMapped]
        public List<GeneralModule> Options { get; set; }
    }
}
