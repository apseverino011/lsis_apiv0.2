using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("CompanyClients")]
    public partial class CompanyClient: EntityBase
    {
        public Guid PartyGID { get; set; }
    }
}
