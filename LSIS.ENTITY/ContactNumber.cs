using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("ContactNumbers")]
    public partial class ContactNumber: EntityBase
    {
        public Guid OwnerId { get; set; }

        public Guid ContactType { get; set; }

        [Required]
        [StringLength(30)]
        public string Number { get; set; }

        [StringLength(5)]
        public string Ext { get; set; }

        public bool IsMain { get; set; }
    }
}
