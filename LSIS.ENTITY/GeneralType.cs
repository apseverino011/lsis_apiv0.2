using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("GeneralTypes")]
    public partial class GeneralType: EntityBase
    {
        [StringLength(15)]
        public string Code { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }
        
        public Guid? OwnerId { get; set; }

        public bool IsParent { get; set; }

        [NotMapped]
        public string StatusName { get; set; }

        [NotMapped]
        public List<GeneralType> Chields { get; set; }
    }
}
