using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("AccRubros")]
    public partial class AccRubro : EntityBase
    {
        [Required]
        [StringLength(5)]
        public string AccRubroCode { get; set; }

        [Required]
        [StringLength(100)]
        public string AccRubroName { get; set; }

        public Guid AccGroupGID { get; set; }

        [NotMapped]
        public AccGroup AccGroup { get; set; }
    }
}
