using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("ContactPersons")]
    public partial class ContactPerson: EntityBase
    {
        public Guid OwnerId { get; set; }

        public Guid ContactId { get; set; }

        [StringLength(12)]
        public string Number { get; set; }

        [StringLength(5)]
        public string Ext { get; set; }

        public bool IsMain { get; set; }

        [NotMapped]
        public Party Contact { get; set; }
    }
}
