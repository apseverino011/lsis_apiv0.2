using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("LoanRequests")]
    public partial class LoanRequest: EntityBase
    {
        public Guid CreateBy { get; set; }

        public Guid TypeGID { get; set; }

        [Required]
        [StringLength(15)]
        public string Number { get; set; }

        public Guid PartyGID { get; set; }

        public decimal Amount { get; set; }

        public short Term { get; set; }

        public Guid TermTypeGID { get; set; }

        public Guid? ApprovedBy { get; set; }

        public DateTime? ApprovedDate { get; set; }

        [StringLength(500)]
        public string Comment { get; set; }

        [NotMapped]
        public List<Warranty> Warranties { get; set; }

        [NotMapped]
        public Party Party { get; set; }
    }
}
