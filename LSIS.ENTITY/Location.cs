using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("Locations")]
    public partial class Location : EntityBase
    {
        public Guid OwnerId { get; set; }

        public Guid? SubSectorGID { get; set; }

        [Required]
        [StringLength(500)]
        public string Adderss { get; set; }

        public short? StreetNumber { get; set; }

        [StringLength(5)]
        public string ZipCode { get; set; }

        [StringLength(24)]
        public string Latitude { get; set; }

        [StringLength(24)]
        public string Longitude { get; set; }

        public bool IsMain { get; set; }
    }
}
