using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("Warranties")]
    public partial class Warranty: EntityBase
    {
        public Guid RequestGID { get; set; }

        public Guid WarrantyTypeGID { get; set; }

        public Guid WarrantyGID { get; set; }

        [NotMapped]
        public Vehicle Vehicle { get; set; }

        [NotMapped]
        public Property Property { get; set; }

        [NotMapped]
        public Party Person { get; set; }
    }
}
