using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("Companies")]
    public partial class Company : EntityBase
    {
        public Guid GroupGID { get; set; }

        [Required]
        [StringLength(15)]
        public string RNC { get; set; }

        [Required]
        [StringLength(15)]
        public string Prefix { get; set; }

        [Required]
        [StringLength(100)]
        public string BusinessName { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        [Required]
        [StringLength(150)]
        public string Url { get; set; }

        [StringLength(100)]
        public string Small_Logo_Path { get; set; }

        [StringLength(100)]
        public string Med_Logo_Path { get; set; }

        [StringLength(100)]
        public string Large_Logo_Path { get; set; }

        [NotMapped]
        public List<CompanyEmploye> Employes { get; set; }

        [NotMapped]
        public List<CompanyClient> Clients { get; set; }

        [NotMapped]
        public List<ContactNumber> ContactNumbers { get; set; }

        [NotMapped]
        public List<ContactPerson> ContactPersons { get; set; }

        [NotMapped]
        public List<Location> Locations { get; set; }

        [NotMapped]
        public List<GeneralModule> Modules { get; set; }
    }
}
