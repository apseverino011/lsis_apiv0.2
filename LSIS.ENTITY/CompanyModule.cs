using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace LSIS.ENTITY
{
    [Table("CompanyModules")]
    public partial class CompanyModule: EntityBase
    {
        public Guid ModuleId { get; set; }
    }
}
