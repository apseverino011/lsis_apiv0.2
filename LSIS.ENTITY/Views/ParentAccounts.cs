﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.ENTITY.Views
{
    public class ParentAccounts
    {
        public Guid AccountId { get; set; }

        [StringLength(21)]
        public string FullNumber { get; set; }

        [StringLength(100)]
        public string AccName { get; set; }

        public DateTime LogDate { get; set; }
    }
}
