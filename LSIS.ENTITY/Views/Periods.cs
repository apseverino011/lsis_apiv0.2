﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.ENTITY.Views
{
    public class Periods
    {
        public Guid PeriodId { get; set; }

        [StringLength(25)]
        public string PeriodName { get; set; }
        
        [StringLength(10)]
        public string StartDate { get; set; }

        [StringLength(10)]
        public string EndDate { get; set; }

        [StringLength(25)]
        public string PeriodState { get; set; }

        public DateTime RegisterDate { get; set; }
    }
}
