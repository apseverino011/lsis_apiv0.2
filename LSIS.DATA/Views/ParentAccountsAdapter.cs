﻿using LSIS.ENTITY.Views;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA.Views
{
    public partial class ParentAccountsAdapter : EntityTypeConfiguration<ParentAccounts>
    {

        public ParentAccountsAdapter()
        {
            this.HasKey(t => t.AccountId);

            this.ToTable("ParentAccounts");
            this.Property(t => t.AccountId);
            this.Property(t => t.FullNumber);
            this.Property(t => t.AccName);
            this.Property(t => t.LogDate);
        }


    }
}
