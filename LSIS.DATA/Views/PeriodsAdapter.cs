﻿using LSIS.ENTITY.Views;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA.Views
{
    public partial class PeriodsAdapter : EntityTypeConfiguration<Periods>
    {
        public PeriodsAdapter()
        {
            this.HasKey(t => t.PeriodId);


            this.ToTable("Periods");
            this.Property(t => t.PeriodId);
            this.Property(t => t.PeriodName);
            this.Property(t => t.StartDate);
            this.Property(t => t.EndDate);
            this.Property(t => t.PeriodState);
            this.Property(t => t.RegisterDate);
        }
    }
}
