﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class AccPeriodDetailsAdapter : AdapterBase<AccPeriodDetails>
    {
        #region Static Properties
        public static AccPeriodDetailsAdapter Current { get { return new AccPeriodDetailsAdapter(); } }
        #endregion

        public AccPeriodDetailsAdapter()
        {
            this.Property(t => t.PeriodId);
            this.Property(t => t.Name);
            this.Property(t => t.PeriodState);
        }
    }
}
