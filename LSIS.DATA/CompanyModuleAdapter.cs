﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class CompanyModuleAdapter:AdapterBase<CompanyModule>
    {
        #region [ Static Properties ]
        public static CompanyModuleAdapter Current { get { return new CompanyModuleAdapter(); } }
        #endregion

        public CompanyModuleAdapter()
        {
            // Properties


            // Table & Column Mappings
            this.Property(t => t.ModuleId).HasColumnName("ModuleId");
        }

    }
}
