﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class AccCatalogAdapter: AdapterBase<AccCatalog>
    {
        #region Static Properties
        public static AccCatalogAdapter Current { get { return new AccCatalogAdapter(); } }
        #endregion

        public AccCatalogAdapter()
        {
            // Properties
            this.Property(t => t.AccName)
                .HasMaxLength(100);

            this.Property(t => t.AccNumber)
                .HasMaxLength(15);


            // Table & Column Mappings
            this.Property(t => t.AccNumber).HasColumnName("AccNumber");
            this.Property(t => t.AccName).HasColumnName("AccName");
            this.Property(t => t.AccFullNumber).HasColumnName("AccFullNumber");
            this.Property(t => t.AccGroupId).HasColumnName("AccGroupId");
            this.Property(t => t.AccClassId).HasColumnName("AccClassId");
            this.Property(t => t.AccRubroId).HasColumnName("AccRubroId");
            this.Property(t => t.AccOriginId).HasColumnName("AccOriginId");
            this.Property(t => t.IsGeneralLedgerAcc).HasColumnName("IsGeneralLedgerAcc");
            this.Property(t => t.IsBalanceSheetAcc).HasColumnName("IsBalanceSheetAcc");
            this.Property(t => t.AccBalanceSheetFunctionId).HasColumnName("AccBalanceSheetFunctionId");
            this.Property(t => t.IsProfitAndLossAcc).HasColumnName("IsProfitAndLossAcc");
            this.Property(t => t.IsCashFlowAcc).HasColumnName("IsCashFlowAcc");
            this.Property(t => t.ParentAccountId).HasColumnName("ParentAccountId");
        }
    }
}
