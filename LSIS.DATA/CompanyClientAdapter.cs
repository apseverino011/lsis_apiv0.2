﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class CompanyClientAdapter:AdapterBase<CompanyClient>
    {
        #region [ Static Properties ]
        public static CompanyClientAdapter Current { get { return new CompanyClientAdapter(); } }
        #endregion

        public CompanyClientAdapter()
        {
            // Properties


            // Table & Column Mappings
            this.Property(t => t.PartyGID).HasColumnName("PartyGID");
        }
    }
}
