﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class AccPeriodsAdapter : AdapterBase<AccPeriods>
    {
        #region Static Properties
        public static AccPeriodsAdapter Current { get { return new AccPeriodsAdapter(); } }
        #endregion

        public AccPeriodsAdapter()
        {
            this.Property(t => t.Name);
            this.Property(t => t.Description);
            this.Property(t => t.StartDate);
            this.Property(t => t.EndDate);
            this.Property(t => t.PeriodState);
        }
    }
}
