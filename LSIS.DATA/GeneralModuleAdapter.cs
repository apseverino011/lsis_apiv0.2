﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class GeneralModuleAdapter:AdapterBase<GeneralModule>
    {
        #region [Static Properties]
        public static GeneralModuleAdapter Current { get { return new GeneralModuleAdapter(); } }
        #endregion

        public GeneralModuleAdapter()
        {
            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(35);

            this.Property(t => t.Link)
                .HasMaxLength(75);


            // Table & Column Mappings
            this.Property(t => t.Sort).HasColumnName("Sort");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Link).HasColumnName("Link");
            this.Property(t => t.IsMain).HasColumnName("IsMain");
            this.Property(t => t.IsSubM).HasColumnName("IsSubM");
            this.Property(t => t.Owner).HasColumnName("Owner");
        }
    }
}
