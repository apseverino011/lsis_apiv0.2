﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class PropertyAdapter : AdapterBase<Property>
    {
        #region [Statis Property]
        public static PropertyAdapter Current { get { return new PropertyAdapter(); } }
        #endregion

        public PropertyAdapter()
        {
            // Properties


            // Table & Column Mappings
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Leves).HasColumnName("Leves");
            this.Property(t => t.Meters).HasColumnName("Meters");
            this.Property(t => t.Address).HasColumnName("Address"); 
            this.Property(t => t.HasPool).HasColumnName("HasPool");
        }
    }
}
