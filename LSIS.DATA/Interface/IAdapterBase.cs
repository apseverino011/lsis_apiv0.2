﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LSIS.UTILITY;

namespace LSIS.DATA.Interface
{
    public interface IAdapterBase<TEntity> where TEntity : EntityBase
    {
        List<TEntity> GetAll(bool isConstruct = false);
        OperationResult<IEnumerable<TEntity>> Save(TEntity entity);
        OperationResult<IEnumerable<TEntity>> Save(IEnumerable<TEntity> entities);

    }
}
