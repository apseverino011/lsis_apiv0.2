﻿using LSIS.ENTITY;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace LSIS.DATA
{
    public partial class HistoricalChangeAdapter : EntityTypeConfiguration<HistoricalChange>
    {
        public HistoricalChangeAdapter()
        {
            this.HasKey(t => t.Indx);

            // Properties
            this.Property(t => t.TableName)
                .HasMaxLength(150);

            this.Property(t => t.PropertyChanged)
                .HasMaxLength(100);


            // Table & Column Mappings
            this.ToTable((new HistoricalChange()).GetTableName());
            this.Property(t => t.GID).HasColumnName("GID");
            this.Property(t => t.Indx).HasColumnName("Indx");
            this.Property(t => t.IsDeleted).HasColumnName("IsDeleted");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.LogDate).HasColumnName("LogDate");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.TableName).HasColumnName("TableName");
            this.Property(t => t.RowId).HasColumnName("RowId");
            this.Property(t => t.PropertyChanged).HasColumnName("PropertyChanged");
            this.Property(t => t.OldValue).HasColumnName("OldValue");
            this.Property(t => t.NewValue).HasColumnName("NewValue");
        }
    }
}
