﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class LoanRequestAdapter: AdapterBase<LoanRequest>
    {
        #region [Static Properties]
        public static LoanRequestAdapter Current { get { return new LoanRequestAdapter(); } }
        #endregion

        public LoanRequestAdapter()
        {
            // Properties
            this.Property(t => t.Number)
                .HasMaxLength(15);

            this.Property(t => t.Comment)
                .HasMaxLength(500);


            // Table & Column Mappings
            this.Property(t => t.CreateBy).HasColumnName("CreateBy");
            this.Property(t => t.TypeGID).HasColumnName("TypeGID");
            this.Property(t => t.Number).HasColumnName("Number");
            this.Property(t => t.PartyGID).HasColumnName("PartyGID");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Term).HasColumnName("Term");
            this.Property(t => t.TermTypeGID).HasColumnName("TermTypeGID");
            this.Property(t => t.ApprovedBy).HasColumnName("ApprovedBy");
            this.Property(t => t.ApprovedDate).HasColumnName("ApprovedDate");
            this.Property(t => t.Comment).HasColumnName("Comment");
        }
    }
}
