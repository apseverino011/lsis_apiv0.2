﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class AccBankAdapter: AdapterBase<AccBank>
    {
        #region Static property
        public static AccBankAdapter Current { get { return new AccBankAdapter(); } }
        #endregion

        public AccBankAdapter()
        {
            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(25);

            this.Property(t => t.Description)
                .HasMaxLength(250);


            // Table & Column Mappings
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
        }
    }
}
