﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class PartyIdentificationAdapter: AdapterBase<PartyIdentification>
    {
        #region [ Static Properties ]
        public static PartyIdentificationAdapter Current { get { return new PartyIdentificationAdapter(); } }
        #endregion

        public PartyIdentificationAdapter()
        {
            // Properties
            this.Property(t => t.Number)
                .HasMaxLength(30);


            // Table & Column Mappings
            this.Property(t => t.Party).HasColumnName("Party");
            this.Property(t => t.IdentificationType).HasColumnName("IdentificationType");
            this.Property(t => t.Number).HasColumnName("Number");
            this.Property(t => t.SubSectorGID).HasColumnName("SubSectorGID");
            this.Property(t => t.ExpirationDate).HasColumnName("ExpirationDate");
        }
    }
}
