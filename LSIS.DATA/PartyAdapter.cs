﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class PartyAdapter: AdapterBase<Party>
    {
        #region [ Static Properties ]
        public static PartyAdapter Current { get { return new PartyAdapter(); } }
        #endregion

        public PartyAdapter()
        {

            // Table & Column Mappings
            this.Property(t => t.FirstName);
            this.Property(t => t.LastName);
            this.Property(t => t.FullName);
            this.Property(t => t.Gender);
            this.Property(t => t.MaritalStatus);
            this.Property(t => t.DoB);
            this.Property(t => t.Email);
        }
    }
}
