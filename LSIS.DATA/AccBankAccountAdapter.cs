﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class AccBankAccountAdapter: AdapterBase<AccBankAccount>
    {
        #region Static Property
        public static AccBankAccountAdapter Current { get { return new AccBankAccountAdapter(); } }
        #endregion

        public AccBankAccountAdapter()
        {
            // Table & Column Mappings
            this.Property(t => t.BankId);
            this.Property(t => t.Type);
            this.Property(t => t.BankAccountNumber);
        }
    }
}
