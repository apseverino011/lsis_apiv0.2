﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class CompanyAdapter: AdapterBase<Company>
    {
        #region [ Static Properties ]
        public static CompanyAdapter Current { get { return new CompanyAdapter(); } }
        #endregion

        //Entity Construct and mapping virtual class with db entity
        public CompanyAdapter()
        {
            // Properties
            this.Property(t => t.RNC)
                .HasMaxLength(15);

            this.Property(t => t.Prefix)
                .HasMaxLength(15);

            this.Property(t => t.BusinessName)
                .HasMaxLength(100);

            this.Property(t => t.Description)
                .HasMaxLength(150);

            this.Property(t => t.Url)
                .HasMaxLength(150);

            this.Property(t => t.Small_Logo_Path)
                .HasMaxLength(100);

            this.Property(t => t.Med_Logo_Path)
                .HasMaxLength(100);

            this.Property(t => t.Large_Logo_Path)
                .HasMaxLength(100);



            // Table & Column Mappings
            this.Property(t => t.RNC).HasColumnName("RNC");
            this.Property(t => t.Prefix).HasColumnName("Prefix");
            this.Property(t => t.BusinessName).HasColumnName("BusinessName");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Url).HasColumnName("Url");
            this.Property(t => t.GroupGID).HasColumnName("GroupGID");
            this.Property(t => t.Small_Logo_Path).HasColumnName("Small_Logo_Path");
            this.Property(t => t.Med_Logo_Path).HasColumnName("Med_Logo_Path");
            this.Property(t => t.Large_Logo_Path).HasColumnName("Large_Logo_Path");

        }
    }
}
