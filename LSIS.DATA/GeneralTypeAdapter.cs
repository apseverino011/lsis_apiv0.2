﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class GeneralTypeAdapter : AdapterBase<GeneralType>
    {
        #region [Status Properties]
        public static GeneralTypeAdapter Current { get { return new GeneralTypeAdapter(); } }
        #endregion

        public GeneralTypeAdapter()
        {
            // Properties
            this.Property(t => t.Code)
                .HasMaxLength(15);

            this.Property(t => t.Description)
                .HasMaxLength(100);


            // Table & Column Mappings
            this.Property(t => t.Code).HasColumnName("Code");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.OwnerId).HasColumnName("OwnerId");
            this.Property(t => t.IsParent).HasColumnName("IsParent");
        }
    }
}
