﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class WarrantyAdapter : AdapterBase<Warranty>
    {

        #region [Static Properties]
        public static WarrantyAdapter Current { get { return new WarrantyAdapter(); } }
        #endregion

        public WarrantyAdapter()
        {
            // Properties


            // Table & Column Mappings
            this.Property(t => t.RequestGID).HasColumnName("RequestGID");
            this.Property(t => t.WarrantyTypeGID).HasColumnName("WarrantyTypeGID");
            this.Property(t => t.WarrantyGID).HasColumnName("WarrantyGID");
        }
    }
}
