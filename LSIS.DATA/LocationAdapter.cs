﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class LocationAdapter: AdapterBase<Location>
    {

        #region [Static Properties]
        public static LocationAdapter Current { get { return new LocationAdapter(); } }
        #endregion

        public LocationAdapter()
        {
            // Properties
            this.Property(t => t.Adderss)
                .HasMaxLength(500);

            this.Property(t => t.ZipCode)
                .HasMaxLength(5);

            this.Property(t => t.Latitude)
                .HasMaxLength(24);

            this.Property(t => t.Longitude)
                .HasMaxLength(24);



            // Table & Column Mappings
            this.Property(t => t.OwnerId).HasColumnName("OwnerId");
            this.Property(t => t.SubSectorGID).HasColumnName("SubSectorGID");
            this.Property(t => t.Adderss).HasColumnName("Adderss");
            this.Property(t => t.StreetNumber).HasColumnName("StreetNumber");
            this.Property(t => t.ZipCode).HasColumnName("ZipCode");
            this.Property(t => t.Latitude).HasColumnName("Latitude");
            this.Property(t => t.Longitude).HasColumnName("Longitude");
            this.Property(t => t.IsMain).HasColumnName("IsMain");
        }

    }
}
