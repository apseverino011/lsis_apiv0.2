﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class ContactPersonsAdapter: AdapterBase<ContactPerson>
    {
        #region [ Static Properties ]
        public static ContactPersonsAdapter Current { get { return new ContactPersonsAdapter(); } }
        #endregion

        //Entity Construct and mapping virtual class with db entity
        public ContactPersonsAdapter()
        {
            // Properties



            // Table & Column Mappings
            this.Property(t => t.OwnerId).HasColumnName("OwnerId");
            this.Property(t => t.ContactId).HasColumnName("ContactId");
            this.Property(t => t.Number).HasColumnName("Number");
            this.Property(t => t.Ext).HasColumnName("Ext");
            this.Property(t => t.IsMain).HasColumnName("IsMain");
        }
    }
}
