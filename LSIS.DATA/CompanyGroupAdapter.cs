﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class CompanyGroupAdapter : AdapterBase<CompanyGroup>
    {
        public static CompanyGroupAdapter Current { get { return new CompanyGroupAdapter(); } }

        //Entity Construct and mapping virtual class with db entity
        public CompanyGroupAdapter()
        {
            // Table & Column Mappings
            this.Property(t => t.Name);
            this.Property(t => t.Description);

        }
    }
}
