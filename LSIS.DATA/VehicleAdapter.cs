﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class VehicleAdapter : AdapterBase<Vehicle>
    {
        #region [Static Properties]
        public static VehicleAdapter Current { get; set; }
        #endregion

        public VehicleAdapter()
        {
            // Properties
            this.Property(t => t.Color)
                .HasMaxLength(15);

            this.Property(t => t.LicensePlate)
                .HasMaxLength(8);

            this.Property(t => t.VIN)
                .HasMaxLength(45);

            this.Property(t => t.Enrollment)
                .HasMaxLength(10);


            // Table & Column Mappings
            this.Property(t => t.BrandGID).HasColumnName("BrandGID");
            this.Property(t => t.ModelGID).HasColumnName("ModelGID");
            this.Property(t => t.Color).HasColumnName("Color");
            this.Property(t => t.Year).HasColumnName("Year");
            this.Property(t => t.LicensePlate).HasColumnName("LicensePlate");
            this.Property(t => t.VIN).HasColumnName("VIN");
            this.Property(t => t.Enrollment).HasColumnName("Enrollment");
        }
    }
}
