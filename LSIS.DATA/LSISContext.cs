﻿using LSIS.DATA.Views;
using LSIS.ENTITY;
using LSIS.ENTITY.Views;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public class LSISContext : DbContext
    {
        static LSISContext()
        {
            Database.SetInitializer<LSISContext>(null);
        }

        public LSISContext()
            : base("Name=LsisContext")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<CompanyGroup> CompanyGroups { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<CompanyEmploye> CompanyEmployees { get; set; }
        public DbSet<CompanyClient> CompanyClients { get; set; }
        public DbSet<CompanyModule> CompanyModules { get; set; }
        public DbSet<Party> Parties { get; set; }
        public DbSet<ContactNumber> PartyContactNumbers { get; set; }
        public DbSet<PartyIdentification> PartyIdentifications { get; set; }
        public DbSet<UsersRole> UsersRoles { get; set; }
        public DbSet<SessionHistory> SessionHistory { get; set; }
        public DbSet<RoleModulesOption> RoleModulesOptions { get; set; }
        public DbSet<HistoricalChange> HistoricalChanges { get; set; }
        public DbSet<GeneralModule> GeneralModules { get; set; }
        public DbSet<GeneralType> GeneralTypes { get; set; }
        public DbSet<LoanRequest> LoanRequests { get; set; }
        public DbSet<Warranty> Warranties { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Property> Properties { get; set; }
        public DbSet<Location> PartyLocations { get; set; }
        public DbSet<ContactPerson> PartyContactPersons { get; set; }
        public DbSet<AccBank> AccBanks { get; set; }
        public DbSet<AccBankAccount> AccBankAccounts { get; set; }
        public DbSet<AccGroup> AccGroups { get; set; }
        public DbSet<AccRubro> AccRubros { get; set; }
        public DbSet<AccCatalog> AccCatalogs { get; set; }
        public DbSet<AccPeriods> AccPeriods { get; set; }
        public DbSet<AccPeriodDetails> AccPeriodDetails { get; set; }
        public DbSet<PeriodState> PeriodState { get; set; }


        //Views
        public DbSet<ParentAccounts> ParentAccounts { get; set; }
        public DbSet<Periods> Periods { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserAdapter());
            modelBuilder.Configurations.Add(new CompanyGroupAdapter());
            modelBuilder.Configurations.Add(new CompanyAdapter());
            modelBuilder.Configurations.Add(new CompanyEmployeAdapter());
            modelBuilder.Configurations.Add(new CompanyClientAdapter());
            modelBuilder.Configurations.Add(new CompanyModuleAdapter());
            modelBuilder.Configurations.Add(new PartyAdapter());
            modelBuilder.Configurations.Add(new ContactNumberAdapter());
            modelBuilder.Configurations.Add(new PartyIdentificationAdapter());
            modelBuilder.Configurations.Add(new UsersRoleAdapter());
            modelBuilder.Configurations.Add(new SessionHistoryAdapter());
            modelBuilder.Configurations.Add(new RoleModulesOptionAdapter());
            modelBuilder.Configurations.Add(new HistoricalChangeAdapter());
            modelBuilder.Configurations.Add(new GeneralModuleAdapter());
            modelBuilder.Configurations.Add(new GeneralTypeAdapter());
            modelBuilder.Configurations.Add(new LoanRequestAdapter());
            modelBuilder.Configurations.Add(new WarrantyAdapter());
            modelBuilder.Configurations.Add(new VehicleAdapter());
            modelBuilder.Configurations.Add(new PropertyAdapter());
            modelBuilder.Configurations.Add(new LocationAdapter());
            modelBuilder.Configurations.Add(new ContactPersonsAdapter());
            modelBuilder.Configurations.Add(new AccBankAdapter());
            modelBuilder.Configurations.Add(new AccBankAccountAdapter());
            modelBuilder.Configurations.Add(new AccGroupAdapter());
            modelBuilder.Configurations.Add(new AccClassAdapter());
            modelBuilder.Configurations.Add(new AccRubroAdapter());
            modelBuilder.Configurations.Add(new AccCatalogAdapter());
            modelBuilder.Configurations.Add(new AccPeriodsAdapter());
            modelBuilder.Configurations.Add(new AccPeriodDetailsAdapter());
            modelBuilder.Configurations.Add(new PeriodStateAdapter());

            //Views
            modelBuilder.Configurations.Add(new ParentAccountsAdapter());
            modelBuilder.Configurations.Add(new PeriodsAdapter());

        }
    }
}
