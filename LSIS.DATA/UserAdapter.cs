﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class UserAdapter : AdapterBase<User>
    {
        #region [ Static Properties ]
        public static UserAdapter Current { get { return new UserAdapter(); } }
        #endregion

        public UserAdapter()
        {
            // Properties

            this.Property(t => t.FullName)
                .HasMaxLength(200);

            this.Property(t => t.UsrId)
                .HasMaxLength(20);

            this.Property(t => t.Language)
                .HasMaxLength(5);


            // Table & Column Mappings
            this.Property(t => t.PartyGID).HasColumnName("PartyGID");
            this.Property(t => t.FullName).HasColumnName("FullName");
            this.Property(t => t.UsrId).HasColumnName("UsrId");
            this.Property(t => t.PssId).HasColumnName("PssId");
            this.Property(t => t.Language).HasColumnName("Language");
            this.Property(t => t.UserRoleGID).HasColumnName("UserRoleGID");
            this.Property(t => t.ExpirationDate).HasColumnName("ExpirationDate");
        }
    }
}
