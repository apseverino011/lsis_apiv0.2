﻿using LSIS.DATA.Interface;
using LSIS.ENTITY;
using LSIS.UTILITY;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Design.PluralizationServices;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.Validation;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace LSIS.DATA
{
    public partial class AdapterBase<TEntity> : EntityTypeConfiguration<TEntity>, IAdapterBase<TEntity>
        where TEntity : EntityBase, new()
    {

        #region [ private properties]

        private LSISContext db;

        private string entityName = (new TEntity()).GetTableName();

        #endregion

        #region [Construct]

        public AdapterBase()
        {
            // Primary Key
            this.HasKey(t => t.Indx);

            // Table & Column Mappings
            this.ToTable(entityName);
            this.Property(t => t.GID);
            this.Property(t => t.Indx);
            this.Property(t => t.IsDeleted);
            this.Property(t => t.IsActive);
            this.Property(t => t.Status);
            this.Property(t => t.LogDate);
            this.Property(t => t.CompanyId);
            this.Property(t => t.UserId);

        }

        #endregion

        #region [ Basic Mathods ]

        public virtual List<TEntity> Find(Expression<Func<TEntity, bool>> condition)
        {
            List<TEntity> entities = new List<TEntity>();
            try
            {
                using (db = new LSISContext())
                {
                    //using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
                    //{
                    var tblObjects = from row in ((IObjectContextAdapter)db).ObjectContext.CreateQuery<TEntity>("[" + entityName + "]").Where<TEntity>(condition).Where(x => !x.IsDeleted).OrderBy(x => x.Indx) select row;

                    if (tblObjects.Any())
                    {
                        foreach (var tbl in tblObjects)
                        {
                            TEntity te = ObjectUtility.FillProperties<TEntity>(tbl);

                            entities.Add(te);
                        }
                    }

                    //    scope.Complete();
                    //}
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                db.Dispose();
            }

            return entities;
        }

        public virtual List<TEntity> GetAll(bool isConstruct = false)
        {
            List<TEntity> entities = new List<TEntity>();

            try
            {
                using (db = new LSISContext())
                {
                    //using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
                    //{

                    var tblObjects = from row in ((IObjectContextAdapter)db).ObjectContext.CreateQuery<TEntity>("[" + entityName + "]").Where(x => !x.IsDeleted).OrderBy(x => x.Indx) select row;

                    if (tblObjects.Any())
                    {
                        foreach (var tbl in tblObjects)
                        {
                            TEntity te = ObjectUtility.FillProperties<TEntity>(tbl);

                            entities.Add(te);
                        }
                    }

                    ////    scope.Complete();
                    ////}
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                db.Dispose();
            }

            return entities;
        }

        public virtual OperationResult<IEnumerable<TEntity>> Save(TEntity entity)
        {
            return this.Save(new List<TEntity>() { entity });
        }

        public virtual OperationResult<IEnumerable<TEntity>> Save(IEnumerable<TEntity> entities)
        {
            OperationResult<IEnumerable<TEntity>> result = new OperationResult<IEnumerable<TEntity>>();
            List<TEntity> data = new List<TEntity>();

            if (!entities.Any()) { return result; }

            using (db = new LSISContext())
            {
                try
                {
                    foreach (var item in entities)
                    {
                        TEntity tblObject = ObjectUtility.FillProperties<TEntity>(item as TEntity);

                        if (item.Indx == 0)
                        {
                            //Set Default properties
                            if (tblObject.GID == null || tblObject.GID == Guid.Empty) { tblObject.GID = Guid.NewGuid(); }
                            tblObject.LogDate = DateTime.Now;
                            if (InternalInfo.UserId != null) { tblObject.UserId = InternalInfo.UserId; }
                            if (InternalInfo.CompanyId != null) { tblObject.CompanyId = InternalInfo.CompanyId; }
                            tblObject.IsActive = true;
                            tblObject.IsDeleted = false;
                            //======================================

                            data.Add(db.Set<TEntity>().Add(tblObject));
                        }
                        else
                        {
                            ((IObjectContextAdapter)db).ObjectContext.AttachTo(entityName, tblObject);
                            ((IObjectContextAdapter)db).ObjectContext.ObjectStateManager.ChangeObjectState(tblObject, System.Data.Entity.EntityState.Modified);

                            //Save properties Chaged on Entity
                            this.SavePropertiesChanged<TEntity>(tblObject as TEntity);

                        }
                    }

                    if (db.SaveChanges() > 0)
                    {
                        result.Success = true;
                        result.Data = data;
                        return result;
                    }
                }
                catch (Exception ex)
                {
                    result.SetFail(ex);
                    //throw ex;
                }

                //catch (DbEntityValidationException e)
                //{
                //    foreach (var eve in e.EntityValidationErrors)
                //    {
                //        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                //            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                //        foreach (var ve in eve.ValidationErrors)
                //        {
                //            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                //                ve.PropertyName, ve.ErrorMessage);
                //        }
                //    }
                //    throw;
                //}

                return result;
            }
        }

        public virtual void ExecProcedureNotReturnData(string procedureName)
        {
            using (db = new LSISContext())
            {
                try
                {
                    ((IObjectContextAdapter)db).ObjectContext.ExecuteStoreCommand(procedureName);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public virtual List<T> CallSelectStoredProcedure<T>(string procedureName) where T : class, new()
        {
            using (db = new LSISContext())
            {
                try
                {
                    return ((IObjectContextAdapter)db).ObjectContext.ExecuteStoreQuery<T>(procedureName).ToList();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        #endregion

        #region [ Private Methods ]

        //Method for save history changed
        private void SavePropertiesChanged<T>(T objNew)
        {
            //Get list of properties
            Type mObjType = objNew.GetType();
            System.Reflection.PropertyInfo[] mObjPropertiesInfo = mObjType.GetProperties();
            //===================================================

            List<HistoricalChange> hCambios = new List<HistoricalChange>();

            try
            {
                using (LSISContext t = new LSISContext())
                {

                    if (entityName == "SessionHistory") { return; }

                    Guid gid = (Guid)objNew.GetType().GetProperty("GID").GetValue(objNew, null);

                    var objDB = (from row in ((IObjectContextAdapter)t).ObjectContext.CreateQuery<TEntity>("[" + objNew.ToString() + "]").Where<TEntity>(x => x.GID == gid)
                                 select row).FirstOrDefault();

                    foreach (System.Reflection.PropertyInfo info in mObjPropertiesInfo)
                    {
                        //Get property new value
                        var newValue = objNew.GetType().GetProperty(info.Name).GetValue(objNew, null);

                        //Get property actual value
                        var oldValue = objDB.GetType().GetProperty(info.Name).GetValue(objDB, null);

                        //Lists property changed
                        if ((newValue != null && oldValue != null) && newValue.ToString() != oldValue.ToString())
                        {
                            hCambios.Add(new HistoricalChange()
                            {

                                TableName = entityName,
                                RowId = (Guid)objDB.GetType().GetProperty("GID").GetValue(objDB, null),
                                PropertyChanged = info.Name,
                                OldValue = Convert.ToString(oldValue),
                                NewValue = Convert.ToString(newValue),
                                UserId = Guid.Empty,
                                LogDate = DateTime.Now


                            });
                        }
                    }

                    t.HistoricalChanges.AddRange(hCambios);
                    t.SaveChanges();
                }


            }
            catch (Exception ex)
            {
                //throw ex;
            }

            //return hCambios;

        }

        #endregion [ Private Methods ]
    }
}
