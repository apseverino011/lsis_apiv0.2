﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class SessionHistoryAdapter : AdapterBase<SessionHistory>
    {
        #region [Static Properties]
        public static SessionHistoryAdapter Current { get { return new SessionHistoryAdapter(); } }
        #endregion

        public SessionHistoryAdapter()
        {
            // Properties
            this.Property(t => t.Ip)
                .HasMaxLength(16);

            this.Property(t => t.MacAddress)
                .HasMaxLength(25);


            // Table & Column Mappings
            this.Property(t => t.Ip).HasColumnName("Ip");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.EndDate).HasColumnName("EndDate");
            this.Property(t => t.SessionTime).HasColumnName("SessionTime");
            this.Property(t => t.LastUpdate).HasColumnName("LastUpdate");
            this.Property(t => t.MacAddress).HasColumnName("MacAddress");
        }
    }
}
