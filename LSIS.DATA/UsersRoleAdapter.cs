﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class UsersRoleAdapter:AdapterBase<UsersRole>
    {
        #region [Static Properties]
        public static UsersRoleAdapter Current { get { return new UsersRoleAdapter(); } }
        #endregion

        public UsersRoleAdapter()
        {
            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(35);

            this.Property(t => t.Description)
                .HasMaxLength(150);


            // Table & Column Mappings
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
        }
    }
}
