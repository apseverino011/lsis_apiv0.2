﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class PeriodStateAdapter: AdapterBase<PeriodState>
    {
        #region Static properties
        public static PeriodStateAdapter Current { get { return new PeriodStateAdapter(); } }
        #endregion

        public PeriodStateAdapter()
        {
            this.Property(t => t.Code);
            this.Property(t => t.Name);
            this.Property(t => t.Description);
        }
    }
}
