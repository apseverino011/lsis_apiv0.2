﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class AccRubroAdapter: AdapterBase<AccRubro>
    {
        #region Static Propertities
        public static AccRubroAdapter Current { get { return new AccRubroAdapter(); } }
        #endregion

        public AccRubroAdapter()
        {
            // Properties
            this.Property(t => t.AccRubroName)
                .HasMaxLength(100);


            // Table & Column Mappings
            this.Property(t => t.AccRubroCode).HasColumnName("AccRubroCode");
            this.Property(t => t.AccRubroName).HasColumnName("AccRubroName");
            this.Property(t => t.AccGroupGID).HasColumnName("AccGroupGID");
        }
    }
}
