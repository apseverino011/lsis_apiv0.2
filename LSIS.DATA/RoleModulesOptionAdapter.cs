﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class RoleModulesOptionAdapter : AdapterBase<RoleModulesOption>
    {
        #region [Static Properties]
        public static RoleModulesOptionAdapter Current { get { return new RoleModulesOptionAdapter(); } }
        #endregion

        public RoleModulesOptionAdapter()
        {
            // Properties


            // Table & Column Mappings
            this.Property(t => t.UserRoleId).HasColumnName("UserRoleId");
            this.Property(t => t.ModuleId).HasColumnName("ModuleId");
            this.Property(t => t.OptionId).HasColumnName("OptionId");
            this.Property(t => t.IsMain).HasColumnName("IsMain");
            this.Property(t => t.IsSub).HasColumnName("IsSub");
            this.Property(t => t._Select).HasColumnName("_Select");
            this.Property(t => t._Update).HasColumnName("_Update");
            this.Property(t => t._Delete).HasColumnName("_Delete");
            this.Property(t => t._Insert).HasColumnName("_Insert");
        }
    }
}
