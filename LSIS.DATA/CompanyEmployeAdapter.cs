﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class CompanyEmployeAdapter:AdapterBase<CompanyEmploye>
    {
        #region [ Static Properties ]
        public static CompanyEmployeAdapter Current { get { return new CompanyEmployeAdapter(); } }
        #endregion


        public CompanyEmployeAdapter()
        {
            // Table & Column Mappings
            this.Property(t => t.CodeEmployee);
            this.Property(t => t.PartyGID);
            this.Property(t => t.Department);
            this.Property(t => t.Position);
            this.Property(t => t.Salary);
        }
    }
}
