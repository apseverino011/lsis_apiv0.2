﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class ContactNumberAdapter: AdapterBase<ContactNumber>
    {
        #region [ Static Properties ]
        public static ContactNumberAdapter Current { get { return new ContactNumberAdapter(); } }
        #endregion

        public ContactNumberAdapter()
        {
            // Properties
            this.Property(t => t.Number)
                .HasMaxLength(30);

            this.Property(t => t.Ext)
                .HasMaxLength(5);


            // Table & Column Mappings
            this.Property(t => t.OwnerId).HasColumnName("OwnerId");
            this.Property(t => t.ContactType).HasColumnName("ContactType");
            this.Property(t => t.Number).HasColumnName("Number");
            this.Property(t => t.Ext).HasColumnName("Ext");
            this.Property(t => t.IsMain).HasColumnName("IsMain");
        }
    }
}
