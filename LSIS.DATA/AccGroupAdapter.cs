﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class AccGroupAdapter: AdapterBase<AccGroup>
    {

        #region Static Property
        public static AccGroupAdapter Current { get { return new AccGroupAdapter(); } }
        #endregion

        public AccGroupAdapter()
        {
            // Properties
            this.Property(t => t.AccGroupName)
                .HasMaxLength(100);


            // Table & Column Mappings
            this.Property(t => t.AccOriginId).HasColumnName("AccOriginId");
            this.Property(t => t.AccGroupCode).HasColumnName("AccGroupCode");
            this.Property(t => t.AccGroupName).HasColumnName("AccGroupName");
            this.Property(t => t.AccTypeId).HasColumnName("AccTypeId");
            this.Property(t => t.NextAccNumber).HasColumnName("NextAccNumber");
            this.Property(t => t.NextSubAccNumber).HasColumnName("NextSubAccNumber");
        }
    }
}
