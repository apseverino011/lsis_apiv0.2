﻿using LSIS.ENTITY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSIS.DATA
{
    public partial class AccClassAdapter : AdapterBase<AccClass>
    {
        #region Static Properties
        public static AccClassAdapter Current { get { return new AccClassAdapter(); } }
        #endregion

        public AccClassAdapter()
        {
            // Properties
            this.Property(t => t.AccClassName)
                .HasMaxLength(100);


            // Table & Column Mappings
            this.Property(t => t.AccClassCode).HasColumnName("AccClassCode");
            this.Property(t => t.AccClassName).HasColumnName("AccClassName");
            this.Property(t => t.AccGroupId).HasColumnName("AccGroupId");
        }
    }
}
